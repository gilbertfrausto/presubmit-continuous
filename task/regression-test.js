'use strict';

const Promise = require('bluebird');
const cmd = require('node-command-line');
const parallel = require('../helper/parallel');
const TaskStore = require('../helper/task-store');
const SummaryStore = require('../helper/summary-store');
const {Omni, Command}= require('../constants');

/**
 * Runs the visual regression test task.
 * @return {!Promise<void|!Array<*>}
 */
const regressionTest = () => {
  /** @const {!SummaryStore} */
  const summaryStore = new SummaryStore('summary-store');

  /** @const {!TaskStore} */
  // todo(gfrausto): Might need the keystore for the constants
  // const keyStore = new TaskStore('key-store');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  // eslint-disable-next-line no-console
  console.log(Command.REGRESSION_TEST, parallel.getCurrentCount());

  return cmd.run(Omni.SUDO_SHELL + Omni.FILES.REGRESSION_TEST)
      .then((res) => {
        summaryStore.updateSummary(/** !SummaryTask */{
          name: Command.REGRESSION_TEST,
          time: new Date().getSeconds() - startTime,
          results: Omni.SUCCESS,
        });
      })
      .catch((err) => {
        summaryStore.updateSummary(/** !SummaryTask */{
          name: Command.REGRESSION_TEST,
          time: new Date().getSeconds() - startTime,
          results: `${Omni.FAILED} ${e}`,
        });
        throw new Error(err);
      });
};

module.exports = regressionTest;
