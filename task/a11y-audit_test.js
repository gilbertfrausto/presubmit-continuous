'use strict';

const Promise = require('bluebird');
const cmd = require('node-command-line');
const sinon = require('sinon');
const {Omni, Command} = require('../constants');
const {expect} = require('chai');
const {before, describe, it, after} = require('mocha');
const logFileDetails = require('../helper/log-file-details');
const a11yAudit = require('./a11y-audit');
const TaskStore = require('../helper/task-store');
const SummaryStore = require('../helper/summary-store');

let cmdStub;
let keyStore;

const success = new Promise((resolve) => {
  resolve({success: true});
});

logFileDetails(__filename, __dirname);

describe('Regression Test file', () => {

  before((done) => {
    sinon.stub(console, 'log');
    keyStore = new TaskStore('key-store');
    cmdStub = sinon.stub(cmd, 'run').returns(success);
    a11yAudit();
    setTimeout(done, 100);
  });

  after(() => {
    // eslint-disable-next-line no-console
    console.log.restore();
    cmd.run.restore();
  });

  it('should run the correct file', () => {
    const fileAndCommand = Omni.SUDO_SHELL + Omni.FILES.A11Y_AUDIT;
    expect(cmdStub.args[0][0]).to.equal(fileAndCommand);
  });

  it('should have updated summary store with results or executed command', () => {
    const summaryStore = SummaryStore.getInstance();
    const results = summaryStore.getSummary().filter((item) => item
        .includes(Command.A11Y_AUDIT));
    expect(results).has.lengthOf(1);
  });
});
