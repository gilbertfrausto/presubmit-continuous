'use strict';

const sinon = require('sinon');
const {Versions, Omni, Command} = require('../constants');
const cmd = require('node-command-line');
const {expect} = require('chai');
const {describe, it, before, after, beforeEach} = require('mocha');
const mastHead = require('./mast-head');
const logFileDetails = require('../helper/log-file-details');
const TaskStore = require('../helper/task-store');
const SummaryStore = require('../helper/summary-store');

/** @type {sinon.SinonSpyStatic|undefined} */
let logger;

let fullMastHead;
let taskStore;
let spy;
let summaryStore;

const STORE_NAME = 'mast-head';
const STORE_NAME_SUMMARY = 'summary-store';

logFileDetails(__filename, __dirname);

describe('Glue command-line masthead -', () => {
  describe('testing with spy', () => {
    before((done) => {
      logger = sinon.spy(console, 'log');
      mastHead(Versions.NPM, false);
      setTimeout(done, 250);
    });
    after(() => {
      console.log.restore();
      taskStore.clear();
    });

    it('should contain correct version numbers', () => {
      // Create store instance
      taskStore = new TaskStore(STORE_NAME);

      // Get store data
      fullMastHead = taskStore.select('mastHead');

      expect(fullMastHead).to.contain(Versions.NPM);
      expect(fullMastHead).to.contain(Versions.NODE);
    });
  });

  // We need to avoid sending this to the console
  describe('testing with stub', () => {
    before(() => {
      logger = sinon.stub(console, 'log');
      taskStore = new TaskStore(STORE_NAME); // Create store instance
      summaryStore = new SummaryStore(STORE_NAME_SUMMARY);
    });
    beforeEach((done)=> {
      summaryStore.clear();
      mastHead(Versions.NPM); // Build and log header
      setTimeout(done, 250);
    })
    after(() => {
      console.log.restore();
      summaryStore.clear();
      taskStore.clear();
    });

    it('should build mast heads with correct version numbers', () => {
      fullMastHead = taskStore.select('mastHead'); // / Get store data
      expect(fullMastHead).to.contain(Versions.NPM);
      expect(fullMastHead).to.contain(Versions.NODE);
    })
    it('should correctly try to log masthead to terminal', () => {
      expect(logger.called).to.equal(true);
      expect(logger.callCount).to.greaterThan(2);
    });

    it('should update summary with results of task', () => {
      const summary = summaryStore.getSummary();
      expect(summary[0]).to.contain(Omni.SUMMARY);
      expect(summary[1]).to.contain(Command.MAST_HEAD);
      expect(summary[1]).to.contain(Omni.SUCCESS);
    })
  });
});
