'use strict';

const cmd = require('node-command-line');
const sinon = require('sinon');
const {Command} = require('../constants');
const {expect} = require('chai');
const {before, describe, it, after} = require('mocha');
const logFileDetails = require('../helper/log-file-details');
const {build} = require('./build');
const TaskStore = require('../helper/task-store');
const ProcessManager = require('../helper/process-manager');

let processManager;
let queueStore;

const queue = {
  pause: sinon.fake(),
};

logFileDetails(__filename, __dirname);

describe('Build task file', () => {

  before((done) => {
    processManager = sinon.stub(ProcessManager, 'getInstance');
    queueStore = new TaskStore('queue-store');
    queueStore.update({queue});
    build();
    setTimeout(done, 100);
  });

  after(() => {
    ProcessManager.getInstance.restore();
  });

  it('should correctly create stores', () => {
    expect(processManager.args[0][0]).to.equal(Command.BUILD);
    expect(processManager.called).to.equal(true);
  });

  it('should correctly pause main queue and create build forks', () => {
    expect(queue.pause.called).to.equal(true);
  });
});


