'use strict';

const Promise = require('bluebird');
const PromiseQueue = require('easy-promise-queue');
const TaskStore = require('../helper/task-store');
const SummaryStore = require('../helper/summary-store');
const {installMainDeps, installSubDeps} = require('./install');
const preBuildTest = require('./pre-build-test');
const unitTest = require('./unit-test');
const regressionTest = require('./regression-test');
const {build, glueMainBuild} = require('./build');
const a11yAudit = require('./a11y-audit');
const linkChecker = require('./link-checker');
const autoDeploy = require('./auto-deploy');


/**
 * PRESUBMIT & Continuous TASK CLASS. this will run only when a presubmit is
 * triggered.
 * -Creates instance of @type {PresubmitContinuous}
 * -Creates promise queue
 * -Updates @type {TaskStore} with queue, resume and pause methods
 * -Prints a command line report by calling @type {SummaryStore.printSummary}
 */
class PresubmitContinuous {

  /**
   * @param {boolean=} presubmit Flag for type of build currently running.
   */
  static getInstance(presubmit = true) {
    return new Promise((resolve, reject) => {
      const presubmit_continuous = new PresubmitContinuous(presubmit);
      try {
        presubmit_continuous.init();
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * @param {boolean} presubmit Flag for type of build currently running
   */
  constructor(presubmit = true) {
    /** @const {boolean} */
    this.presubmit = presubmit;

    /** @private {!TaskStore} */
    this.queueStore_;

    /** @private {!e} */
    this.queue_;

    /** @private {!SummaryStore} */
    this.summaryStore_;
  }

  /**
   * Init properties with values
   */
  init() {
    this.queueStore_ = new TaskStore('queue-store');

    // Update the store with the promise queue
    this.queueStore_.update({
      queue: new PromiseQueue({concurrency: 1}),
    });

    this.queue_ = this.queueStore_.select('queue');

    this.build();
  }

  /**
   * Build up queue and update the queue task store.
   */
  build() {
    // Create queue
    this.createAndStartQueue();

    // Add pause method
    this.queueStore_.update({
      pause: this.queue_.pause,
    });

    // Add resume method
    this.queueStore_.update({
      resume: () => {
        // if only main thread is running, restart the queue
        this.queue_.resume();
      },
    });
  }

  /**
   * Scope issue with passing @type {SummaryStore.printSummary} to promise queue
   * @return {!Promise<*>}
   */
  print() {
    return new Promise((resolve, reject) => {
      try {
        /** @type {!SummaryStore} */
        const summaryStore = SummaryStore.getInstance();
        summaryStore.printSummary();
        resolve();
      } catch (e) {
        reject(e);
      }
    });
  }

  /**
   * Add methods to the queue, will automatically start
   */
  createAndStartQueue() {
    const queue = [
      // installMainDeps,
      // installSubDeps,
      // preBuildTest,
      // glueMainBuild,
      // build,
      // unitTest,
      // regressionTest,
      linkChecker,
    ];

    // These task are only to run under a continuous build
    if (!this.presubmit) {
      queue.push(autoDeploy, linkChecker, a11yAudit);
    }

    // Printing the summary must go last
    queue.push(this.print);

    // Add all task to the queue and it automatically start.
    this.queue_.add(queue);
  }
};


module.exports = PresubmitContinuous;
