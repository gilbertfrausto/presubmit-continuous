'use strict';

const sinon = require('sinon');
const {expect} = require('chai');
const {before, describe, it, after} = require('mocha');
const logFileDetails = require('../helper/log-file-details');
const PresubmitContinuous = require('./presubmit-continuous');
const TaskStore = require('../helper/task-store');
const SummaryStore = require('../helper/summary-store');

// Get the current store.
let presubmit_continuous;
let taskStore;
let queue;
let pause;
let resume;
let summaryStore;

logFileDetails(__filename, __dirname);

describe('Presubmit and Continuous class', () => {
  before(() => {
    presubmit_continuous = new PresubmitContinuous(true);
    summaryStore = sinon.stub(SummaryStore, 'getInstance').returns({
      printSummary: sinon.fake()
    });
  });

  after(() =>{
    SummaryStore.getInstance.restore();
  });

  it('should create correct instance', () => {
    expect(presubmit_continuous instanceof PresubmitContinuous).to.equal(true);
  });

  it('should correctly init properties', () => {
    const build = sinon.stub(presubmit_continuous, 'build');

    presubmit_continuous.init();

    taskStore = new TaskStore('queue-store');
    queue = taskStore.select('queue');

    // check task store, should return currently task store instance
    expect(queue).to.have.property('_queue');
    expect(queue).to.have.property('_concurrency');
    expect(queue).to.have.property('_pause');
    expect(queue['_concurrency']).to.equal(1);
    expect(queue['_pause']).to.equal(false);

    expect(build.called).to.equal(true);
    expect(summaryStore.notCalled).to.equal(true);
  });

  it('should correctly build & trigger queue and update task store', () => {
    presubmit_continuous.build.restore();

    taskStore = new TaskStore('queue-store');
    const startQueue = sinon.stub(presubmit_continuous, 'createAndStartQueue');

    presubmit_continuous.init();

    pause = taskStore.select('pause');
    queue = taskStore.select('queue');
    resume = taskStore.select('resume');

    expect(startQueue.called).to.equal(true);
    expect(pause).to.be.a('function');
    expect(resume).to.be.a('function');
    expect(queue._pause).to.equal(false);
    expect(summaryStore.notCalled).to.equal(true);
  });

  it('should call PresubmitContinuous.print should call Summary.print', () => {
    presubmit_continuous.print();
    const printSummary = summaryStore.firstCall.returnValue.printSummary;
    expect(summaryStore.called).to.equal(true);
    expect(summaryStore.calledOnce).to.equal(true);
    expect(printSummary.called).to.equal(true);
    expect(printSummary.calledOnce).to.equal(true);
  });
});