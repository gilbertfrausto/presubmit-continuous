'use strict';

const Promise = require('bluebird');
const SummaryStore = require('../helper/summary-store');
const {Command, Omni} = require('../constants');
const TaskStore = require('../helper/task-store');
const ProcessManager = require('../helper/process-manager');
const glueMainBuild = require('../subtask/build/glue-main');

/**
 * Running build Command and updates the summary store with results.
 * @return {Promise<*>}
 */
const build = () => {
  /** @const {!TaskStore} */
  const queueStore = new TaskStore('queue-store');

  /**
   * TODO(gfrausto):minified code so the types are incorrect, need to look into
   * solutions for the PromiseQueue types.
   * @const {e} Need to find or make type for the PromiseQueue
   */
  const queue = queueStore.select('queue');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  return new Promise((resolve, reject) => {
    try {
      queue['pause'] && queue.pause();
      ProcessManager.getInstance(Command.BUILD);
      resolve();
    } catch (e) {
      /** @const {!SummaryStore} */
      const summaryStore = new SummaryStore('summary-store');

      summaryStore.updateSummary(/** !SummaryTask */{
        name: Command.BUILD,
        time: new Date().getSeconds() - startTime,
        results: `${Omni.FAILED} ${e}`,
      });

      reject(e);
    }
  });
};

module.exports = {
  build,
  glueMainBuild,
};
