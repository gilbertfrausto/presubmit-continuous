
'use strict';

const Promise = require('bluebird');
const SummaryStore = require('../helper/summary-store');
const jsLinter = require('../subtask/linter/js-linter');
const scssLinter = require('../subtask/linter/scss-linter');
const {Omni, Command} = require('../constants');

/**
 * Runs the Pre-test tasks, which includes all linter tasks.
 * @return {!Promise<void|!Array<*>}
 */
const preBuildTest = () => {
  /** @const {!SummaryStore} */
  const summaryStore = new SummaryStore('summary-store');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  /** @const {!Array<!Promise<void>>} */
  const task = [jsLinter(), scssLinter()];

  return Promise.all(task).spread((a, b) => {
    summaryStore.updateSummary(/** !SummaryTask */{
      name: Command.PRE_TEST,
      time: new Date().getSeconds() - startTime,
      results: Omni.SUCCESS,
    });
  }).catch((e) => {
    summaryStore.updateSummary(/** !SummaryTask */{
      name: Command.PRE_TEST,
      time: new Date().getSeconds() - startTime,
      results: `${Omni.FAILED} ${e}`,
    });
  });
};

module.exports = preBuildTest;
