'use strict';

const Promise = require('bluebird');
const cmd = require('node-command-line');
const SummaryStore = require('../helper/summary-store');
const parallel = require('../helper/parallel');
const {Omni, Command} = require('../constants');
const SiteChecker = require('../helper/site-checker');

/** @const {Array<string>} */
const APP_LINKS = Omni.LINKS;

/**
 * Runs a link checker
 * @return {Promise<*>} This is called via async function, so we need to return
 */
const linkChecker = () => {
  /** @const {!SummaryStore} */
  const summaryStore = new SummaryStore('summary-store');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  // eslint-disable-next-line no-console
  console.log(Command.LINK_CHECKER, parallel.getCurrentCount());

  /** @const {Array<Promise<*>} */
  const allSites = [];

  /**
   * Get an instance of link checker for each site in the @type {Omni.LINKS}
   * Array.
   */
  APP_LINKS.forEach((item) => {
    const siteChecker = SiteChecker.getInstance(item);
    allSites.push(siteChecker.init());
  });

  return Promise.all(allSites).then((res) => {
      summaryStore.updateSummary(/** !SummaryTask */{
        name: Command.LINK_CHECKER,
        time: new Date().getSeconds() - startTime,
        results: Omni.SUCCESS,
      });
    })
    .catch((err) => {
      summaryStore.updateSummary(/** !SummaryTask */{
        name: Command.LINK_CHECKER,
        time: new Date().getSeconds() - startTime,
        results: `${Omni.FAILED} ${e}`,
      });
      throw new Error(err);
    });
};

module.exports = linkChecker;
