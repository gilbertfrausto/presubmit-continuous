'use strict';

const cmd = require('node-command-line');
const sinon = require('sinon');
const {Command} = require('../constants');
const {expect} = require('chai');
const Promise = require('bluebird');
const {before, describe, it, after, beforeEach} = require('mocha');
const linkChecker = require('../task/link-checker');
const logFileDetails = require('../helper/log-file-details');
const SiteChecker = require('../helper/site-checker');
const SummaryStore = require('../helper/summary-store');

let stub;
let instance;

const siteCheckerInstance = {
  init: sinon.fake()
};

logFileDetails(__filename, __dirname);

describe('Link Checker task file', () => {
  before(() => {
    sinon.stub(console, 'log');
    sinon.stub(SiteChecker, 'getInstance').returns(siteCheckerInstance);
    sinon.spy(Promise, 'all');
  });

  beforeEach(() => {
    instance = linkChecker();
  });

  after(() => {
    SiteChecker.getInstance.restore();
    Promise.all.restore();
    console.log.restore();
  });

  it('should correctly get site checker instances', (done) => {
    expect(instance).to.be.instanceOf(Promise);
    expect(siteCheckerInstance.init.called).to.equal(true);
    expect(Promise.all.called).to.equal(true);
    done();
  });

  it('should have correct results in summary', () => {
    const summaryStore = SummaryStore.getInstance();
    const results = summaryStore.getSummary().filter((item) => item
        .includes(Command.LINK_CHECKER));
    expect(results).has.lengthOf(1);
  });
});


