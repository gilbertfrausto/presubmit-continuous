'use strict';

const Promise = require('bluebird');
const cmd = require('node-command-line');
const parallel = require('../helper/parallel');
const SummaryStore = require('../helper/summary-store');
const {Omni, Command}= require('../constants');

/**
 * Runs the visual regression test task.
 * @return {!Promise<void|!Array<*>}
 */
const autoDeploy = () => {
  /** @const {!SummaryStore} */
  const summaryStore = new SummaryStore('summary-store');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  // eslint-disable-next-line no-console
  console.log(Command.AUTO_DEPLOY, parallel.getCurrentCount());

  return cmd.run(Omni.SUDO_SHELL + Omni.FILES.AUTO_DEPLOY)
      .then((res) => {
        summaryStore.updateSummary(/** !SummaryTask */{
          name: Command.AUTO_DEPLOY,
          time: new Date().getSeconds() - startTime,
          results: Omni.SUCCESS,
        });
      })
      .catch((err) => {
        summaryStore.updateSummary(/** !SummaryTask */{
          name: Command.AUTO_DEPLOY,
          time: new Date().getSeconds() - startTime,
          results: `${Omni.FAILED} ${e}`,
        });
        throw new Error(err);
      });
};

module.exports = autoDeploy;
