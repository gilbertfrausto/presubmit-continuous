'use strict';

const {Omni, Command} = require('../constants');
const TaskStore = require('../helper/task-store');
const SummaryStore = require('../helper/summary-store');

/**
 * Prints the mast head containing Node and NPM version numbers.
 * @param {string} npm_v npm version since it is not available on the global
 *    object.
 * @param {boolean=} log Flag for printing log to the terminal.
 * @return {Promise<*>} This is called via async function, so we need to return
 *    a promise.
 */
const mastHead = (npm_v, log = true) => {
  /** @const {number} */
  const startTime = new Date().getSeconds();

  /** @const {string} */
  const NODE_V = process.version;

  /** @const {string} */
  const NPM_V = npm_v;

  /** @const {!TaskStore} */
  const taskStore = new TaskStore('mast-head');

  /** @const {!SummaryStore} */
  const summaryStore = new SummaryStore('summary-store');

  /**
   * This is hard-coded here because its needs version numbers which won't be
   * available in the constants file
   * @const {string}
   */
  const fullMastHead = `
  *************************************************************************

                                🌟 Glue 🌟

              Starting build with Node ${NODE_V} and npm ${NPM_V}

  *************************************************************************
  `;

  // add task to the store.
  taskStore.update({
    mastHead: fullMastHead,
  });

  return new Promise((resolve, reject) => {
    try {
      if (log) {
        console.log(fullMastHead); // eslint-disable-line no-console
        console.log(Omni.START_TASK); // eslint-disable-line no-console
      }

      summaryStore.updateSummary(/** !SummaryTask */{
        name: Command.MAST_HEAD,
        time: new Date().getSeconds() - startTime,
        results: Omni.SUCCESS,
      });
      resolve();
    } catch (e) {
      summaryStore.updateSummary(/** !SummaryTask */{
        name: Command.MAST_HEAD,
        time: new Date().getSeconds() - startTime,
        results: `${Omni.FAILED} ${e}`,
      });
      reject(e);
    }
  });
};

module.exports = mastHead;
