'use strict';

const Promise = require('bluebird');
const SummaryStore = require('../helper/summary-store');
const jsUnitTest = require('../subtask/unit-test/js-unit-test');
const scssUnitTest = require('../subtask/unit-test/scss-unit-test');
const cdnUnitTest = require('../subtask/unit-test/cdn-unit-test');
const {Omni, Command}= require('../constants');

/**
 * Executes the Glue/Hercules unit-test commands
 */
const unitTest = () => {
  /** @const {!SummaryStore} */
  const summaryStore = new SummaryStore('summary-store');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  /** @const {!Array<!Promise<void>>}*/
  const task = [jsUnitTest(), scssUnitTest(), cdnUnitTest()];

  return Promise.all(task).spread((a, b) => {
    summaryStore.updateSummary(/** !SummaryTask */{
      name: Command.UNIT_TEST,
      time: new Date().getSeconds() - startTime,
      results: Omni.SUCCESS,
    });
  }).catch((e) => {
    summaryStore.updateSummary(/** !SummaryTask */{
      name: Command.UNIT_TEST,
      time: new Date().getSeconds() - startTime,
      results: `${Omni.FAILED} ${e}`,
    });
  });
};

module.exports = unitTest;
