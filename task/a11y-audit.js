'use strict';

const Promise = require('bluebird');
const cmd = require('node-command-line');
const parallel = require('../helper/parallel');
const TaskStore = require('../helper/task-store');
const SummaryStore = require('../helper/summary-store');
const {Omni, Command}= require('../constants');

/**
 * Runs the visual regression test task.
 * @return {!Promise<void|!Array<*>}
 */
const a11yAudit = () => {
  /** @const {!SummaryStore} */
  const summaryStore = new SummaryStore('summary-store');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  // eslint-disable-next-line no-console
  console.log(Command.A11Y_AUDIT, parallel.getCurrentCount());

  return cmd.run(Omni.SUDO_SHELL + Omni.FILES.A11Y_AUDIT)
      .then((res) => {
        summaryStore.updateSummary(/** !SummaryTask */{
          name: Command.A11Y_AUDIT,
          time: new Date().getSeconds() - startTime,
          results: Omni.SUCCESS,
        });
      })
      .catch((err) => {
        summaryStore.updateSummary(/** !SummaryTask */{
          name: Command.A11Y_AUDIT,
          time: new Date().getSeconds() - startTime,
          results: `${Omni.FAILED} ${e}`,
        });
        throw new Error(err);
      });
};

module.exports = a11yAudit;
