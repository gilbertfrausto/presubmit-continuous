'use strict';

const sinon = require('sinon');
const {Versions} = require('../constants');
const cmd = require('node-command-line');
const child_process = require('child_process');
const {expect} = require('chai');
const {afterEach, beforeEach, describe, it} = require('mocha');
const {installSubDeps} = require('./install');
const logFileDetails = require('../helper/log-file-details');
const TaskStore = require('../helper/task-store');
const ProcessStore = require('../helper/process-store');
const parallel = require('../helper/parallel');
const forks = require('../helper/process-manager');

// Get the current store.
let install;
let installInstance;
let taskStore;
let child;
let directoryTaskStore;

const changed = {
  'GLUE_DOCS': 'GLUE_DOCS',
  'GLUE_DEMOS': 'GLUE_DEMOS',
  'HERCULES_DOCS': 'HERCULES_DOCS',
  'HERCULES_DEMOS': 'HERCULES_DEMOS',
};

logFileDetails(__filename, __dirname);

describe('Installing deps for main and sub directory', () => {
  beforeEach((done) => {
    parallel.setProcessCount(0);

    directoryTaskStore = new TaskStore('git-store');
    directoryTaskStore.update({changed});

    child = sinon.stub(child_process, 'fork').returns({
      on: (prop) => {
        sinon.fake();
      }
    });

    taskStore = new TaskStore('queue-store');

    taskStore.update({
      queue: {
        pause: () => {
          sinon.fake();
        }
      }
    });

    install = installSubDeps();

    installInstance = new ProcessStore('child-store');

    setTimeout(done, 1500);
  });

  afterEach(() => {
    install = undefined;
    parallel.setProcessCount(0);
    child_process.fork.restore();
  })

  it('should have correct properties', () => {
    expect(parallel.getCurrentCount()).to.equal(4);
    expect(taskStore.select()).to.have.property('name');
    expect(directoryTaskStore.select()).to.have.property('changed');
    // expect(installInstance.select()).to.have.property('name');
    expect(installInstance.select()).to.have.property('forked');
  });

  // Will update unit test once done with updates
  it('should call correct method to setup communication on child threads', (done) => {
    expect(parallel.getCurrentCount()).to.equal(4);
    expect(install).to.be.a('Promise');
    expect(taskStore.select()).to.have.property('queue');
    expect(taskStore.select()['queue']).to.have.property('pause');
    done();
  });
});
