'use strict';

const child_process = require('child_process');
const glueMain = require('../subtask/install/glue-main');
const parallel = require('../helper/parallel');
const TaskStore = require('../helper/task-store');
const ProcessStore = require('../helper/process-store');
const directoryGitChanges = require('../helper/directory-git-changes');
const ProcessReport = require('../helper/process-report');
const ProcessManager = require('../helper/process-manager');
const SummaryStore = require('../helper/summary-store');
const {Task, Omni, Command} = require('../constants');

directoryGitChanges();

/**
 * Installs all sub directories with is dependencies. Also runs these task in
 * parallel.
 * @return {!Promise<*>}
 */
const installSubDeps = () => {
  /** @const {!TaskStore} */
  const taskStore = new TaskStore('queue-store');

  /** @const {e} */
  const queue = taskStore.select('queue');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  // Return promise (needed for queue)
  return new Promise((resolve, reject) => {
    try {
      queue['pause'] && queue.pause();
      ProcessManager.getInstance(Command.INSTALL);
      resolve();
    } catch (e)  {
      summaryStore.updateSummary({
        name: 'Install Sub Dependencies',
        time: new Date().getSeconds() - startTime,
        results: `Failed ${e}`,
      });
      reject(e);
    }
  });
};

module.exports = {
  installSubDeps,
  installMainDeps: glueMain,
};
