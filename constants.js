'use strict';

/**
 * Each directory will have a list of commands to be executed. Also each
 * directory will have an array of Javascript files the need to be executed
 * on a sub-process.
 *
 * @typedef {{
 *   MAST_HEAD=: string,
 *   GIT_CHANGE=: string,
 *   INSTALL=: string,
 *   BUILD=: string,
 *   PRE_TEST_JS=: string,
 *   PRE_TEST_SCSS=: string,
 *   UNIT_TEST_JS=: string,
 *   UNIT_TEST_SCSS=: string,
 *   UNIT_TEST_CDN=: string,
 *   REGRESSION_TEST=: string,
 *   NODE_UNIT_TEST=: string,
 *   AUTO_DEPLOY=: string,
 *   LINK_CHECKER=: string,
 *   A11Y_AUDIT=: string,
 *   NPM_VERSION=: string,
 * }}
 */
let Commands;

/**
 * Each directory will have a list of commands to be executed. Also each
 * directory will have an array of Javascript files the need to be executed
 * on a sub-process.
 *
 * @typedef {{
 *   COMMANDS: Commands,
 *   FILES: Commands,
 * }}
 */
let Directory;

const path = (process.env.NODE_ENV === 'Production') ? 'TBD' : '../../..';

/**
 * @enum {string}
 */
const GitCommand = {
  DIFF: 'git diff --quiet HEAD~ --',
  LAST_AUTHOR: `git log --format='%ae' -n 1 | cut -d@ -f1`,
  LAST_COMMIT_HASH: `git log --format="%H" -n 1`,
};

/**
 * The value for @type {Process.env.NODE_ENV} is set by package.json.
 * @enum {string}
 */
const Versions = {
  NPM: (process.env.NODE_ENV === 'Production') ? '6.1.0.' : '5.3.0',
  NODE: 'v8.4.0',
};

/** @enum {string} */
const Command = {
  MAST_HEAD: 'MAST_HEAD',
  GIT_CHANGE: 'GIT_CHANGE',
  INSTALL: 'INSTALL',
  BUILD: 'BUILD',
  PRE_TEST: 'PRE_TEST',
  UNIT_TEST: 'UNIT_TEST',
  REGRESSION_TEST: 'REGRESSION_TEST',
  NODE_UNIT_TEST: 'NODE_UNIT_TEST',
  AUTO_DEPLOY: 'AUTO_DEPLOY',
  LINK_CHECKER: 'LINK_CHECKER',
  A11Y_AUDIT: 'A11Y_AUDIT',
  NPM_VERSION: 'NPM_VERSION',
};

/**
 * todo(gfrausto): update paths to a variable. For running on the local machine
 * and server
 * @const {Object<string, string|undefined|!Array<string>>}
 */
const Omni = {
  COMMANDS: {
    PRE_TEST_JS: `cd ${path} && yarn run lint:js`,
    PRE_TEST_SCSS: `cd ${path}  && yarn run lint:sass`,
    UNIT_TEST_JS: `cd ${path} && yarn run test:unit`,
    UNIT_TEST_SCSS: `cd ${path} . && yarn run test:css-unit`,
    UNIT_TEST_CDN: `cd ${path}  && yarn run test:cdn`,
    LINK_CHECKER: `cd ${path}  && yarn run test:link-checker-prod-all`,
    NPM_VERSION: 'npm -v',
  },
  FILES: {
    REGRESSION_TEST: './shell-scripts/regression-test.sh',
    A11Y_AUDIT: './shell-scripts/a11y-audit.sh',
    AUTO_DEPLOY: './shell-scripts/auto-deploy.sh',
  },

  // ROOT PERMISSIONS
  SUDO_SHELL: 'chmod +x * && sh ',

  // KOKORO
  KOKORO_GERRIT_CHANGE_NUMBER: undefined,
  KOKORO_KEYSTORE_DIR: undefined,

  // PERCY
  PERCY_PROJECT: 'Google-Glue-team/glue',
  PERCY_TARGET_BRANCH: 'master',

  // OTHER
  START_TASK: '🧛 TASK NOW RUNNING 🧛',
  SUMMARY: '\nSummary Report \n',
  SPACER: '\n',
  FAILED: 'Failed:',
  SUCCESS: 'Completed Successfully',
  DONE: '🧛🧛  TASK NOW COMPLETE 🧛🧛',
  PRESUBMIT: 'presubmit',
  CONTINUOUS: 'continuous',
  REGRESSION_MAST: '\n\n*** 🐡 VISUAL REGRESSION TESTS 🐡  ***',

  // Events
  ON_EXIT: 'exit',
  ON_DISCONNECT: 'disconnect',
  ON_MESSAGE: 'message',
  LINKS: [
    'https://glue-demo.appspot.com',
    'https://glue-docs.appspot.com',
    'https://hercules-demo.appspot.com',
    'https://hercules-docs.appspot.com',
  ],
};

/**
 * @const {Object<string, Directory>}
 */
const MainDirectory = {
  GLUE_MAIN: {
    COMMANDS: {
      INSTALL: `cd ${path} && yarn install`,
      BUILD: `cd ${path} && yarn run build`,
    },
    FILES: {
      INSTALL: './subtask/install/glue-main',
      BUILD: './subtask/build/glue-main',
    },
  },
};

/**
 * @const {Object<string, Directory>}
 */
const DemosDocsDirectories = {
  GLUE_DOCS: {
    COMMANDS: {
      GIT_CHANGE: 'docs',
      INSTALL: `cd ${path}/docs && yarn install`,
      BUILD: `cd ${path}/docs && yarn run build`,
    },
    FILES: {
      GIT_CHANGE: 'docs',
      INSTALL: './subtask/install/demos-and-docs',
      BUILD: './subtask/build/demos-and-docs',
    },
  },
  GLUE_DEMOS: {
    COMMANDS: {
      GIT_CHANGE: `cdn/src lib demos scss ':(exclude)lib/*_test.js`,
      INSTALL: `cd ${path}/demos && yarn install`,
      BUILD: `cd ${path}/demos && yarn run build`,
    },
    FILES: {
      INSTALL: './subtask/install/demos-and-docs',
      BUILD: './subtask/build/demos-and-docs',
      NODE_UNIT_TEST: 'helper/node-child-process',
    },
  },
  HERCULES_DOCS: {
    COMMANDS: {
      GIT_CHANGE: 'hercules/docs',
      INSTALL: `cd ${path}/hercules/docs && yarn install`,
      BUILD: `cd ${path}/hercules/docs && yarn run build`,
    },
    FILES: {
      INSTALL: './subtask/install/demos-and-docs',
      BUILD: './subtask/build/demos-and-docs',
    },
  },
  HERCULES_DEMOS: {
    COMMANDS: {
      GIT_CHANGE: 'cdn/src lib scss hercules/lib hercules/demos',
      INSTALL: `cd ${path}/hercules/demos && yarn install`,
      BUILD: `cd ${path}/hercules/demos && yarn run build`,
    },
    FILES: {
      INSTALL: './subtask/install/demos-and-docs',
      BUILD: './subtask/build/demos-and-docs',
    },
  },
};

/** @enum {string} */
const NodeColors = {
  RESET: '\x1b[0m',
  BRIGHT: '\x1b[1m',
  DIM: '\x1b[2m',
  UNDERSCORE: '\x1b[4m',
  BLINK: '\x1b[5m',
  REVERSE: '\x1b[7m',
  HIDDEN: '\x1b[8m',

  UNDERSCORE: '\x1b[4m',
  BLINK: '\x1b[5m',
  REVERSE: '\x1b[7m',
  HIDDEN: '\x1b[8m',

  FG_BLACK: '\x1b[30m',
  FG_RED: '\x1b[31m',
  FG_GREEN: '\x1b[32m',
  FG_YELLOW: '\x1b[33m',
  FG_BLUE: '\x1b[34m',
  FG_MAGENTA: '\x1b[35m',
  FG_CYAN: '\x1b[36m',
  FG_WHITE: '\x1b[37m',

  BG_BLACK: '\x1b[40m',
  BG_RED: '\x1b[41m',
  BG_GREEN: '\x1b[42m',
  BG_YELLOW: '\x1b[43m',
  BG_BLUE: '\x1b[44m',
  BG_MAGENTA: '\x1b[45m',
  BG_CYAN: '\x1b[46m',
  BG_WHITE: '\x1b[47m',
};

module.exports = {
  GitCommand,
  Command,
  Omni,
  Versions,
  MainDirectory,
  DemosDocsDirectories,
  NodeColors,
};
