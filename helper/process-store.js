const Store = require('./store');


/**
 * This store only to process and sub-process data saved.
 */
class ProcessStore extends Store {

  /**
   * @param {string} name
   */
  constructor(name) {
    super(name);
  }

  /**
   * If you wish to cancel a process must pass its PID.
   * @param {string} id Process id that is needed to terminate process.
   */
  cancelProcess(id) {
    const forkedProcess = id ? this.select('forked') : undefined;
    if (forkedProcess && id) {
      forkedProcess[id].kill();
    } else {
      if (!id) {
        throw new Error(`Must include PID of process you wish to kill
        if you want to terminate all sub-processes using {ProcessStore.cancelAll}
        `);
      } else {
        throw new Error(`No process with id ${id} can be found`);
      }
    }
  }

  /**
   * Cancel all child processes running and main thread
   * @param {boolean=} log Will log the results if true.
   */
  cancelAll(log = true) {
    const allForkedProcesses = this.select('forked');
    if (allForkedProcesses) {
      for (let forkedProcess in allForkedProcesses) {
        if (allForkedProcesses.hasOwnProperty(forkedProcess) &&
            forkedProcess !== 'name') {
          // eslint-disable-next-line no-console
          log && console.log(`Process terminated: ${forkedProcess}`);
          allForkedProcesses[forkedProcess].kill();
        }
      }
    }

    log && console.log('cancel all fired'); // eslint-disable-line no-console

    process.exit(); // cancel main
  }
}


module.exports = ProcessStore;
