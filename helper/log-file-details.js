'use strict';

/**
 * Small helper method to show what files have been tested.
 * @param {string} file
 * @param {string} dir
 */
const logFileDetails = (file, dir) => {
  console.log(file.slice(dir.length + 1)); // eslint-disable-line no-console
};

module.exports = logFileDetails;
