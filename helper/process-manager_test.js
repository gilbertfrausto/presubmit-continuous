;'use strict';

const sinon = require('sinon');
const {Command, Omni, DemosDocsDirectories} = require('../constants');
const {expect} = require('chai');
const {describe, it, before, after, beforeEach, afterEach} = require('mocha');
const logFileDetails = require('./log-file-details');
const ProcessManager = require('./process-manager');
const child_process = require('child_process');
const parallel = require('./parallel');
const TaskStore = require('./task-store');
const ProcessStore = require('./process-store');
const SummaryStore = require('./summary-store');

let gitStore;
let consoleStub;
let consoleErrorStub;
let instance;
let allForks;
let childStore;
let summaryStore;
let queueStore;

const error = Error;
global.Error = sinon.fake();

logFileDetails(__filename, __dirname);

const STORE_NAME = 'child-store';
const GIT_STORE_NAME = 'git-store';
const SUMMARY_STORE_NAME = 'summary-store';
const QUEUE_STORE = 'queue-store';

const queue = {
  resume: sinon.fake(),
};

const disStr = '\n    Disconnected process: GLUE_DEMOS,\n    Process count: 1\n    ';

describe('Create forks from Child Process', () => {
  describe('testing events - error', () => {
    /** @type {Object<string, string>} */
    let changed = {
      GLUE_DEMOS: 'GLUE_DEMOS',
    };
    before(() => {
      consoleStub = sinon.stub(console, 'log');
      consoleErrorStub = sinon.stub(console, 'error');

      queueStore = new TaskStore(QUEUE_STORE);
      gitStore = new TaskStore(GIT_STORE_NAME);
      childStore = new ProcessStore(STORE_NAME);
      summaryStore = new SummaryStore(SUMMARY_STORE_NAME);

      gitStore.update({changed});
      queueStore.update({queue});
    });

    beforeEach(() => {
      parallel.setProcessCount(1);
    });

    afterEach(() => {
      gitStore.clear();
    })

    after(() => {
      console.log.restore();
      console.error.restore();
      global.Error = error;
    });

    it('should should trigger an error', (done) => {
      ProcessManager
          .getInstance(Command.NODE_UNIT_TEST, Omni.ON_EXIT);

      setTimeout(() => {
        expect(global.Error.called).to.equal(true);
        expect(consoleErrorStub.called).to.equal(true);
        expect(consoleErrorStub.calledOnce).to.equal(true);
        done();
      }, 1000);

    });
  });

  describe('testing fork creation', () => {
    /** @type {Object<string, string>} */
    let changed = {
      GLUE_DOCS: 'GLUE_DOCS',
      GLUE_DEMOS: 'GLUE_DEMOS',
    };
    before(() => {
      gitStore = new TaskStore(GIT_STORE_NAME);
      childStore = new ProcessStore(STORE_NAME);
      summaryStore = new SummaryStore('summary-store');
      stub = sinon.stub(child_process, 'fork').returns({
        pid: Math.random(),
        on: sinon.fake(),
        kill: sinon.fake(),
      });
      gitStore.update({changed});
      allForks = childStore.select('forked');
    });

    beforeEach(() => {
      parallel.setProcessCount(1);
      instance = ProcessManager.getInstance(Command.INSTALL);
      allForks = childStore.select('forked');
    });

    after(() => {
      childStore.clear();
      child_process.fork.restore();
    });

    it('should have a correct list of directories store in git-store', () => {
      expect(gitStore.select('name')).to.equal(GIT_STORE_NAME);
    });

    it('should correctly create forks', () => {
      expect(stub.called).to.equal(true);
      expect(stub.callCount).to.equal(4);
      expect(allForks[changed['GLUE_DEMOS']]).to.have.property('on');
      expect(allForks[changed['GLUE_DEMOS']]).to.have.property('kill');
      expect(allForks[changed['GLUE_DEMOS']]).to.have.property('pid');
      expect(allForks[changed['GLUE_DOCS']]).to.have.property('on');
      expect(allForks[changed['GLUE_DOCS']]).to.have.property('kill');
      expect(allForks[changed['GLUE_DOCS']]).to.have.property('pid');
      expect(parallel.getCurrentCount()).to.equal(3);
    });

    it('should have received same object from task store', () => {
      expect(instance).to.be.instanceOf(ProcessManager);
      expect(instance.getForkedProcesses()).to.be.a('object');
      expect(instance.getForkedProcesses()).to.equal(allForks);
      expect(parallel.getCurrentCount()).to.equal(3);
    });
  });

});

describe('Create forks from Child Process', () => {
  describe('testing events message', () => {
    /** @type {Object<string, string>} */
    let changed = {
      GLUE_DEMOS: 'GLUE_DEMOS',
    };
    before(() => {
      consoleStub = sinon.stub(console, 'log');
      consoleErrorStub = sinon.stub(console, 'error');

      queueStore = new TaskStore(QUEUE_STORE);
      gitStore = new TaskStore(GIT_STORE_NAME);
      childStore = new ProcessStore(STORE_NAME);
      summaryStore = new SummaryStore(SUMMARY_STORE_NAME);

      gitStore.update({changed});
      queueStore.update({queue});
    });

    after(() => {
      console.log.restore();
      console.error.restore();
      global.Error = error;
    });

    beforeEach(() => {
      parallel.setProcessCount(1);
    });

    afterEach(() => {
      gitStore.clear();
    });

    it('should trigger message event and send back a message', (done) => {
      instance = ProcessManager
          .getInstance(Command.NODE_UNIT_TEST, Omni.ON_MESSAGE);

      setTimeout(() => {
        expect(consoleStub.args[0].includes(Omni.ON_MESSAGE)).to.equal(true);
        done();
      }, 1000);

    });
``
  });

  describe('testing events - disconnect', () => {
    /** @type {Object<string, string>} */
    let changed = {
      GLUE_DEMOS: 'GLUE_DEMOS',
    };

    beforeEach(() => {
      parallel.setProcessCount(1);
    });

    afterEach(() => {
      gitStore.clear();
    })

    before(() => {
      consoleStub = sinon.stub(console, 'log');
      consoleErrorStub = sinon.stub(console, 'error');

      queueStore = new TaskStore(QUEUE_STORE);
      gitStore = new TaskStore(GIT_STORE_NAME);
      childStore = new ProcessStore(STORE_NAME);
      summaryStore = new SummaryStore(SUMMARY_STORE_NAME);

      gitStore.update({changed});
      queueStore.update({queue});
    });

    after(() => {
      console.log.restore();
      console.error.restore();
      global.Error = error;
    });

    it('should trigger disconnect event and resume queue', (done) => {
      ProcessManager
          .getInstance(Command.NODE_UNIT_TEST, Omni.ON_DISCONNECT);

      setTimeout(() => {
        const count = parallel.getCurrentCount();
        expect(count).to.equal(1);
        expect(queue.resume.called).to.equal(true);
        expect(consoleStub.args[0].includes(disStr)).to.equal(true);
        done();
      }, 1000);

    });

  });

});