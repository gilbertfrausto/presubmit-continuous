'use strict';

const {NodeColors} = require('../constants');
const BLC = require('broken-link-checker');


/**
 * For clarity this will be its own class. Calls the BLC API to allow for more
 * flexibility with options. Also returns a promise, as is need for interacting
 * with the promise queue.
 * @final
 */
class SiteChecker {

  /**
   * Using a static method helps with unit testing.
   * @param {string} link Url of site to be checked
   * @return {SiteChecker}
   */
  static getInstance(link) {
    return new SiteChecker(link);
  }

  /**
   * @param {string} link Url of site to be checked
   * @return Promise<*>
   */
  constructor(link) {
    /** @const {string} */
    this.link = link;

    /** @type {BLC.SiteChecker} */
    this.siteCheckerInstance;
  }

  /**
   * Calls @type {SiteCheck.breakingLinkChecker_} and wraps it with a promise.
   * @return Promise<*>
   */
  init() {
    return new Promise((resolve, reject) => {
      this.breakingLinkChecker_(resolve, reject);
    });
  }

  /**
   * Interacts with the BLC api, passes in options and then calls the enqueue
   * to start link-checker
   * @param {function()} resolve Passed from promise
   * @param {function()} reject Passed from promise
   */
  breakingLinkChecker_(resolve, reject) {
    /** @type {string} */
    const red  = NodeColors.FG_RED;

    /** @type {string} */
    const yellow  = NodeColors.FG_YELLOW;

    /** @type {string} */
    const reset = NodeColors.RESET;

    this.siteCheckerInstance = new BLC.SiteChecker(undefined, {
      link: function(result) {
        if (result.broken) {
          // eslint-disable-next-line no-console
          console.log(`
            Base: ${result.base.original},
            Resolved: ${yellow}${result.url.resolved}${reset},
            Reason: ${red}${result.brokenReason}${reset}
          `);
        }
      },
      page: function(error) {
        if (error) {
          reject();
        }
      },
      end: function() {
        resolve();
      },
    });

    // This will actually start the link checker.
    this.siteCheckerInstance.enqueue(this.link);
  }
}

module.exports = SiteChecker;
