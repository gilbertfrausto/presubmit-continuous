'use strict';

const cmd = require('node-command-line');
const Promise = require('bluebird');
const child_process = require('child_process');
const {GitCommand, DemosDocsDirectories} = require('../constants');
const TaskStore = require('../helper/task-store');

/**
 * A collection of all directories that currently have GIT changes.
 * @typedef {{
 *  changed: Object<string, string|boolean>,
 * }}
 */
let ChangedDirectories;

/**
 * This will go through main and sub-directories and return an object with
 * directories that have changes in them.
 * @return {!ChangedDirectories}
 */
const directoryGitChanges = () => {

  /** @type {!Object<string, string|boolean>} */
  let changed = {};

  /** @type {!TaskStore} */
  let taskStore = new TaskStore('git-store');

  /**
   * TODO(gfrausto):  look into a better way to capture this information Maybe
   * place this in a array and use Promise.spread or Promise.map
   */
  Promise.coroutine(function* () {
    // Loop all sub directories
    for (let dirName in DemosDocsDirectories) {
      if (DemosDocsDirectories.hasOwnProperty(dirName)) {
        if (!changed[dirName]) {
          changed[dirName] = {};
        }

        /** @const {string} */
        const command = DemosDocsDirectories[dirName].COMMANDS.GIT_CHANGE;

        /**
         * Run git diff command on all Hercules and Glue directories
         */
        changed[dirName].response = yield cmd
            .run(`${GitCommand.DIFF} ${command}`);

        // Get status
        changed[dirName] = (changed[dirName].response.success) ? dirName : false;
      }
    }

    // Update store with changes and update flag
    taskStore.update({changed});
  })();

  return Object.freeze({changed});
};

module.exports = directoryGitChanges;
