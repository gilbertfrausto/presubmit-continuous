'use strict';

const sinon = require('sinon');
const {expect} = require('chai');
const {Omni} = require('../constants');
const child_process = require('child_process');
const {describe, it} = require('mocha');
const logFileDetails = require('../helper/log-file-details');
const ProcessStore = require('../helper/process-store');

logFileDetails(__filename, __dirname);

/** @type {ProcessStore} */
let processStore;
let saved;

const STORE_NAME = 'child-Store';
process.exit = sinon.fake();

const mockProcess = {
  0: {
    kill: sinon.fake(),
    on: sinon.fake(),
  },
  1: {
    kill: sinon.fake(),
    on: sinon.fake(),
  }
};

describe('Process Store', () => {
  before(() => {
    processStore = new ProcessStore(STORE_NAME);
  });

  it('should create process store instance', () => {
    expect(processStore).to.be.instanceOf(ProcessStore);
  });

  it('should update the process store with mock forks', () => {
    processStore.update({
      forked: mockProcess
    });
    saved = processStore.select('forked');
    expect(saved).to.have.property('0');
    expect(saved).to.have.property('1');
  });

  it('should cancel process with passed PID', () => {
    processStore.cancelProcess('0');
    expect(mockProcess['0'].kill.called).to.equal(true);
  })
  it('should cancel all processes running and stop main thread', () => {
    processStore.cancelAll(false);
    expect(mockProcess['0'].kill.called).to.equal(true);
    expect(mockProcess['1'].kill.called).to.equal(true);
    expect(process.exit.called).to.equal(true);
  });

  it('should clear process store', () => {
    processStore.clear();
    processStore = new ProcessStore(STORE_NAME);
    saved = processStore.select();

    expect(saved).to.have.property('name');
    expect(Object.keys(saved).length).to.equal(1);
  });

});