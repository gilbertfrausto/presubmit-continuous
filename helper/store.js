'use strict';

/**
 * @typedef {{
 *  store: !WeakMap,
 *  keys: !Object.<string, Object>,
 * }}
 * @extends {NodeJS.Global}
 */
let GlobalObject;

/**
 * @typedef {!Object<string, *>}
 */
let Payload;

/**
 * @typedef {{
 *  name: string,
 *  payload: !Payload,
 * }}
 */
let UpdateObject;


/**
 * Base Store class
 * - creates keys and store properties on the global object
 * - creates WeakMap and methods to update, clear and select it contents.
 */
class Store {

  /**
   * @param {string} name
   */
  constructor(name) {
    /** @protected {string} */
    this.name = name;

    /** @type {!GlobalObject} */
    this.global_ =  /** @type {!GlobalObject} */(global);

    // Set keys properties and init new WeakMap
    this.global_.keys = global.keys || {};
    this.global_.store = global.store ? global.store : new WeakMap();

    // Create store
    this.create_(name);
  }

  /**
   * Get the all every instance of @type {Store} created.
   * @return {WeakMap<!Object, *>}
   */
  get store() {
    return this.global_.store;
  }

  /**
   * Public update method and adding the store location property.
   * @param {!Payload} payload
   */
  update(payload) {
    // Set the new data to the WeakMap
    // Updates the store with a new object. Using @type {Object.assign} we
    // overwrite any properties with the new payload
    this.global_.store.set(this.global_.keys[this.name],
        Object.assign(this.selectStore_(this.name), payload));
  }

  /**
   * Clear store by deleting the key.
   * @param {string=} name The store name.
   */
  clear(name = undefined) {
    this.global_.store.delete(this.global_.keys[name || this.name]);
    delete this.global_.keys[name || this.name];
  }

  /**
   * Select the property that is currently saved in the store. If no arguments
   * is passed, will return all stored items.
   * @param {string=} prop The property you want to fetch from the store.
   * @return {*}
   */
  select(prop = undefined) {
    return prop ? this.selectStore_(this.name)[prop]
        : this.selectStore_(this.name);
  }

  /**
   * This checks to see if the store has already been created. If so it will not
   *  create another one.
   * @param {string} name The name of the store to be create.
   * @private
   */
  create_(name) {
    if (!this.global_.keys.hasOwnProperty(name)) {
      // Create key to the new store,
      // These are unique and do not extend Object.prototype
      this.global_.keys[name] = Object.freeze(Object.create(null));

      // Set the new store the Weak Map
      this.global_.store.set(this.global_.keys[name], {name: name});
    }
  }

  /**
   * Fetches the store saved on the @type {GlobalObject} if its not found we
   * throw an exception.
   * @param {string} name  The name of the store
   * @return {!Object<string, *>}
   * @private
   */
  selectStore_(name) {
    try {
      return this.global_.store.get(this.global_.keys[name]);
    } catch (e) {
      throw new Error(`
        Error: ${e}, possible issue will global store or key to the store.
      `);
    }
  }

  /**
   * Updates the store with a new object. Using @type {Object.assign} we
   * overwrite any properties with the new payload
   * @param {UpdateObject} data Collection data to be saved to the store.
   * @private
   */
  updateStore_(data) {
    const {name, payload} = data;

    // Set the new data to the WeakMap
    this.global_.store.set(this.global_.keys[this.name],
        Object.assign(this.selectStore_(this.name), payload));
  }
}


module.exports = Store;
