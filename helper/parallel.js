'use strict';

/**
 * @typedef {{
 *  getCurrentCount: {function():number},
 *  addProcess: {function()},
 *  removeProcess: {function()},
 *  setProcessCount: {function(number)},
 *  printCount: {function()},
 * }}
 */
let Parallel;

/**
 * Function to keep track off the Parallel child process count.
 * @return {Readonly<!Parallel>}
 */
const parallel = (() => {
  if (!global['processCounter']) {
    global.processCounter = 1;
  }

  /**
   * @return {number}
   */
  function getCurrentCount() {
    return global.processCounter;
  }

  /**
   * Add process to the counter
   */
  function addProcess() {
    global.processCounter++;
  }

  /**
   * Remove a process from the count
   */
  function removeProcess() {
    global.processCounter--;
  }

  /**
   * This is used incase and issues may arise and for unit testing.
   * @param {number} count
   */
  function setProcessCount(count) {
    global.processCounter = count;
  }

  /**
   * Log the current count to the terminal.
   */
  function printCount() {
    // eslint-disable-next-line no-console
    console.log(`Process count: ${global.processCounter}`);
  }

  /**
   * Create a frozen Object with public methods. Creating a 'Prototype-ish'
   * object here because its needed for testing.
   */
  return Object.freeze(/** @type {!Parallel} */({
    getCurrentCount,
    addProcess,
    removeProcess,
    setProcessCount,
    printCount,
  }));
})();

module.exports = parallel;
