'use strict';

const child_process = require('child_process');
const parallel = require('./parallel');
const {DemosDocsDirectories, Task, Omni, Command} = require('../constants');
const TaskStore = require('./task-store');
const ProcessStore = require('./process-store');
const SummaryStore = require('./summary-store');
const ProcessReport = require('./process-report');

/**
 * @typedef {!Object<string, !child_process.ChildProcess>}
 */
let ProcessCollection;


/**
 * This class creates a child process as needed and sets event listeners.
 * The child processes we create will emit events, this helps us set up
 * communication to the main thread.
 *
 * Responsibilities-
 * -Creates a @see {ChildProcess} on directories that have GIT changes
 * -Add Event listeners [error, disconnect, message]
 * -Updates child store with each forked process
 * -Keeps the process count updated
 * -Calls the @type {ProcessReport} for each process to disconnect
 * @final
 */
class ProcessManager {

  static getInstance(filePathIndex, event = undefined) {
    return new ProcessManager(filePathIndex, event);
  }

  /**
   * Index number for js file path.
   *
   * EX:
   * const Command.BUILD = 0;
   * DemosDocsDirectories.GLUE_DEMOS.FILES[Command.BUILD];
   *
   * @param {string} filePathKey Javascript file index.
   * @param {string=} event Name of an event you wish to trigger.
   */
  constructor(filePathKey, event = undefined) {
    /** @private @const {number} */
    this.startTime_ = new Date().getSeconds();

    /** @private @const {!TaskStore} */
    this.gitStore_ =  new TaskStore('git-store');

    /** @private @const {string} */
    this.filePathKey_ = filePathKey;

    /** @private @const {!TaskStore} */
    this.childStore_ = new ProcessStore('child-store');

    /** @private @const {!SummaryStore} */
    this.summaryStore_ = new SummaryStore('summary-store');

    /** @private @const {!TaskStore} */
    this.queueStore_ = new TaskStore('queue-store');

    /** @private @const {string|undefined} */
    this.event_ = event;

    /** @private {!ProcessCollection} */
    this.processCollection_ = {};

    this.createChildProcess_();
  }

  /**
   * A collection of all forked process on this instance.
   * @return {!ProcessCollection}
   */
  getForkedProcesses() {
    return this.processCollection_;
  }

  /**
   * Build index, value comes from constants.js @type{Command}
   * @return {key}
   */
  getBuildKey() {
    return this.filePathKey_;
  }

  /**
   * Get the directories that need child_process.fork. Git-store will return
   * an object with directory names.
   * let directoriesWithChanges = {
   *  GLUE_DOCS: 'GLUE_DOCS',
   *  GLUE_DEMOS: 'GLUE_DEMOS'
   * }
   * @private
   */
  createChildProcess_() {
    /** @type {!Object<string, string>} */
    const directoriesWithChanges = this.gitStore_.select('changed');
    for (let directoryName in directoriesWithChanges) {
      if (directoriesWithChanges.hasOwnProperty(directoryName)) {
        this.createProcessAndEvents_(directoryName);
      }
    }
  }

  /**
   * The directory name passed is used to get the JS path from the FILES array
   * and create a forked process, path must be a js file. This will execute
   * that file on a child process. We also set up the event listeners so when
   * events are emitted the main thread can handle it accordingly.
   * @param {string} name The directory name.
   * @private
   */
  createProcessAndEvents_(name) {
    /** @type {string} */
    const jsFilePath = DemosDocsDirectories[name].FILES[this.filePathKey_];

    // Update the process
    parallel.addProcess();

    // Save a reference of the newly create ChildProcess
    this.processCollection_[name] = child_process.fork(jsFilePath);

    // Add the event listeners
    this.onExit_(name);
    this.onMessage_(name);
    this.onDisconnect_(name);

    // Send directory name, to the main thread
    this.processCollection_[name].send && this.processCollection_[name]
        .send(this.event_ || name);

    // Update the store
    this.childStore_.update({
      forked: this.processCollection_,
    });
  }

  /**
   * Adds an Error event listener on the current forked process. If an error is
   * emitted from a child process, it throws an exception cancels all child
   * processes running.
   * @param {string} name The directory name.
   * @private
   */
  onExit_(name) {
    this.processCollection_[name].on(Omni.ON_EXIT, (exitCode) => {
      console.log('code:', exitCode); // eslint-disable-line no-console
      if (exitCode&& exitCode !== 0) {
        this.childStore_.cancelAll(); // cancel all child process and main
        console.error(`Error: ${exitCode}`);
      }
    });
  }

  /**
   * Adds and event listener for 'message'. This allows the main thread to
   * capture messages emitted from the child process. This also allows us
   * capture process info for reporting and summary data.
   * @param {string} name The directory name.
   * @private
   */
  onMessage_(name) {
    this.processCollection_[name].on(Omni.ON_MESSAGE, (msg) => {
      // Issues with certain NPM/YARN messages triggering the 'message' event.
      // We only need Summary data, some need to check the string to make sure
      // its not an NPM message
      if (msg && msg.includes && msg.includes('{', '}')
          && !msg.includes('&&', 'yarn')) {
        // eslint-disable-next-line no-console
        this.event_ && console.log(Omni.ON_MESSAGE, msg);
        this.summaryStore_.updateSummary(JSON.parse(msg));
      } else {
        // eslint-disable-next-line no-console
        console.log(`Process ${this.processCollection_[name].pid}:`, msg);
      }
    });
  }

  /**
   * Adds an event listener of 'disconnect', which emits from a child process
   * once its completed. This is where we handle process count and resume the
   * queue on the main thread
   * @param {string} name The directory name.
   * @private
   */
  onDisconnect_(name) {
    /** @type {!PromiseQueue} */
    const queue = this.queueStore_.select('queue');

    // Runs when a child process completes
    this.processCollection_[name].on(Omni.ON_DISCONNECT, () => {

      // remove process from the counter)
      parallel.removeProcess();

      if (parallel.getCurrentCount() <= 1) {
        queue.resume(); // try to resume the queue
      }

      // Will Print Process data
      new ProcessReport(name, parallel.getCurrentCount());
    });
  }
}


module.exports = ProcessManager;
