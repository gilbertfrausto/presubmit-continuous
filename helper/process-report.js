'use strict';


/**
 * This is being used to print process details as task are complete.
 * Just for command line updates, no robust like the Summary report.
 */
class ProcessReport {

  /**
   * @param {string} processName process name
   * @param {number} processCount current process count when called
   * @param {boolean=} disconnect process disconnect or connected
   * @param {boolean=} log if user wishes to log details to terminal
   */
  constructor(processName, processCount, disconnect = true, log = true) {
    /** @private @const  {string} */
    this.connection_ = (disconnect) ? 'Disconnected' : 'Connected';

    /** @private @const {string} */
    this.report_ = `
    ${this.connection_} process: ${processName},
    Process count: ${processCount}
    `;

    if (log) {
      console.log(this.report_); // eslint-disable-line no-console
    }
  }

  /**
   * Will return the same report that is printed to the terminal.
   * @return {string}
   */
  getReportDetails() {
    return this.report_;
  }
}


module.exports = ProcessReport;
