'use strict';

const {expect} = require('chai');
const {describe, before, it} = require('mocha');
const Store = require('../helper/store');
const logFileDetails = require('../helper/log-file-details');

// Log file name
logFileDetails(__filename, __dirname);
let store;

const STORE_NAME = 'test';

describe('Store constructor', () => {
  beforeEach(() => {
    store = undefined;
    global['keys'] && delete global['keys'];
    global['store'] && delete global['store'];
  });

  it('should create an instance of store', () => {
    store = new Store(STORE_NAME);
    expect(store.global_).to.equal(global);
  });
  it('should create an instance of store', () => {
    store = new Store(STORE_NAME);
    expect(store instanceof Store).to.equal(true);
  });

  it('should correctly name add key and store instance to global object', () => {
    store = new Store(STORE_NAME);
    const globalStore = global['store'];
    const globalKeys = global['keys'];

    expect(globalStore instanceof WeakMap).to.equal(true);
    expect(globalStore.get(globalKeys[STORE_NAME])).to.not.equal(undefined);
  });

  it('should correctly update and select store data', () => {
    store = new Store(STORE_NAME);

    store.update({
      stuff: '123',
      changes: 'changed'
    });

    expect(store.select('stuff')).to.equal('123');
    expect(store.select('changes')).to.equal('changed');
  });

  it('should correctly create and clear store', () => {
    store = new Store(STORE_NAME);
    const globalStore = global['store'];
    const globalKeys = global['keys'];

    expect(globalStore instanceof WeakMap).to.equal(true);
    expect(globalStore.get(globalKeys[STORE_NAME])).to.not.equal(undefined);

    store.clear();

    expect(globalStore).to.not.have.property(STORE_NAME);
  });
});