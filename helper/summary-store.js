'use strict';
const {Omni, NodeColors} = require('../constants');
const Store = require('./store');

 /**
  * Object with data for summary report
  * @typedef {{
  *   name: string,
  *   time: number,
  *   results: string,
  * }}
  */
 let SummaryTask;


/**
 * Summary store is used to saved the results of all task and subtask.
 * Then print a detailed report to the terminal. Helpfully for debugging but it
 * can be tough to go through logs and find the error this should fix that issue.
 */
class SummaryStore extends Store {

  /**
   * used to print report via Main.js
   * @param {boolean=} presubmit - true if presubmit build.
   * @return {!SummaryStore}
   */
  static getInstance(presubmit = true) {
    return new SummaryStore('summary-store');
  }

  /**
   * @param {string} name Store name.
   */
  constructor(name) {
    super(name);
    this.setup_(); // this method will update the store with a summary array
  }

  /**
   * This will print summary report of all task results to the terminal window.
   */
  printSummary() {
    const allTasks = this.buildSummaryString_(this.select('summary'));

    // eslint-disable-next-line no-console
    allTasks.forEach((item) => console.log(item));
  }

  /**
   * Get an array of the summary strings.
   * @return {!Array<string>}
   */
  getSummary() {
    return this.buildSummaryString_(this.select('summary'));
  }

  /**
   * The main update method uses objects, here we are using and array some we
   *  will wrap the base store method.
   * @param {!SummaryTask} currentTask Object containing summary data for task
   *    that has been completed.
   */
  updateSummary(currentTask) {
    const currentlySaved = /** @type {Array<!SummaryTask>} */
        (this.select('summary'));

    currentlySaved.push(currentTask);

    this.update({
      'summary': currentlySaved,
    });
  }

  /**
   * Must add a summary array before trying to push any task to it.
   * @private
   */
  setup_() {
    this.update({
      'summary': this.select('summary') || [],
    });
  }

  /**
   * Build an array of summary strings to print to command-line.
   * @param {!Array<!SummaryTask>} summaryData A collection of all task and
   *    the results,
   * @return {!Array<string>}
   * @private
   */
  buildSummaryString_(summaryData) {
    return summaryData.reduce((all, item, index) => {
      /** @type {string} */
      const blue  = NodeColors.FG_CYAN;

      /** @type {string} */
      const reset = NodeColors.RESET;

      /** @type {string} */
      const str = `[${index + 1}] ${blue}${item.name}: ${reset}returned in ` +
          `${item.time}.0s, ${item.results}`;

      // ADD HEADER
      (index === 0) && all.push(Omni.SUMMARY);

      // Push summary string
      all.push(str);

      // Push a spacer(line brake) last
      (index === summaryData.length - 1) && all.push(Omni.SPACER, Omni.DONE);

      return all;
    }, []);
  }
}


module.exports = SummaryStore;
