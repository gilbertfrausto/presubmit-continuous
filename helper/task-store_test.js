'use strict';

const {expect} = require('chai');
const {describe, beforeEach, it} = require('mocha');
const TaskStore = require('../helper/task-store');
const logFileDetails = require('../helper/log-file-details');

logFileDetails(__filename, __dirname);

let taskStore;
const STORE_NAME = 'testTask';

describe('Task Store constructor', () => {
  beforeEach(() => {
    taskStore = undefined;
  });

  it('should create an instance of TaskStore', () => {
    taskStore = new TaskStore(STORE_NAME);
    expect(taskStore instanceof TaskStore).to.equal(true);
  });

  it('should return the correct TaskStore name', () => {
    taskStore = new TaskStore(STORE_NAME);
    expect(taskStore.getTaskStoreName()).to.equal(STORE_NAME);
  });

  it('should create and update task correctly', () => {
    let queue;
    taskStore = new TaskStore(STORE_NAME);

    taskStore.update({
      process: 'abc'
    });

    taskStore.update({
      queue: ['task_1', 'task_2', 'task_3']
    });

    queue = taskStore.select('queue');

    expect(taskStore.select('process')).to.equal('abc');
    expect(queue[0]).to.equal('task_1');
    expect(queue[2]).to.equal('task_3');

    taskStore.update({
      process: 'xyz'
    });

    taskStore.update({
      queue: ['task_2', 'task_3']
    });

    queue = taskStore.select('queue');

    expect(taskStore.select('process')).to.equal('xyz');
    expect(queue[0]).to.equal('task_2');
    expect(queue[1]).to.equal('task_3');
  });
});