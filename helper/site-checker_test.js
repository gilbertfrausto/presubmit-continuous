'use strict';

const sinon = require('sinon');
const {Omni, Command} = require('../constants');
const {expect} = require('chai');
const {before, describe, it, after} = require('mocha');
const logFileDetails = require('./log-file-details');
const SiteChecker = require('./site-checker');
const BLC = require('broken-link-checker');

let spy;
let init;
let instance;

const URL = Omni.LINKS[1];

const mockObj = {
  enqueue: sinon.fake(),
};

logFileDetails(__filename, __dirname);

describe('Site Checker Test file', () => {

  before(() => {
    spy = sinon.spy(BLC, 'SiteChecker');
    instance = SiteChecker.getInstance(URL);
  });

  after(() => {
    BLC.SiteChecker.restore();
  })

  it('should have correct instance and url param', () => {
    init = instance.init();
    const args = spy.firstCall.proxy.returnValues[0].currentSiteUrl;
    expect(instance).to.be.instanceOf(SiteChecker);
    expect(spy.called).to.equal(true);
    expect(args).to.equal(URL);
    expect(init).to.be.instanceOf(Promise);
  });

  it('should return correct instance of BLC SiteChecker', () => {
    init = instance.init();
    expect(instance.siteCheckerInstance).to.be.instanceOf(BLC.SiteChecker);
  });
});
