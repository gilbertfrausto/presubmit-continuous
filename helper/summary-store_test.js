'use strict';

const {expect} = require('chai');
const {describe, it, before, after} = require('mocha');
const {Omni} = require('../constants');
const logFileDetails = require('../helper/log-file-details');
const SummaryStore = require('../helper/summary-store');

logFileDetails(__filename, __dirname);

let summary;
let summaryStore;

describe('Summary store - ', () => {
  describe('creating instance with keyword new', () => {
    after(() => {
      summaryStore.clear();
      summaryStore = undefined;
    });

    it('should be presubmit', () => {
      summaryStore = SummaryStore.getInstance();
      expect(summaryStore.name).to.equal('summary-store');
    });

    it('should be continuous', () => {
      summaryStore = SummaryStore.getInstance(false);
      expect(summaryStore.name).to.equal('summary-store');
    });
  });

  describe('creating Summary store with static method', () => {
    before(() => {
      summaryStore = new SummaryStore('summary');
    });

    it('should create summary store instance', () => {
      expect(summaryStore).to.be.instanceOf(SummaryStore);
    });

    it('should update the store with 1 task', () => {
      const testTask = {
        name: 'Task ABC',
        time: new Date().getSeconds(),
        results: Omni.SUCCESS,
      };

      // Update the summary
      summaryStore.updateSummary(testTask);
      summary = summaryStore.getSummary();

      expect(summary[0]).to.contain(Omni.SUMMARY);
      expect(summary[1]).to.contain(testTask.name);
      expect(summary[1]).to.contain(Omni.SUCCESS);
    });

    it('should have 2 task in summary store', () => {
      const testTask_2 = {
        name: 'Task 2',
        time: new Date().getSeconds(),
        results: Omni.SUCCESS,
      };

      // Update the summary
      summaryStore.updateSummary(testTask_2);
      summary = summaryStore.getSummary();

      expect(summary[0]).to.contain(Omni.SUMMARY);
      expect(summary[2]).to.contain(testTask_2.name);
      expect(summary[2]).to.contain(Omni.SUCCESS);
    });

    it('should clear store and return no summary', () => {
      summaryStore.clear();
      summaryStore = new SummaryStore('summary');
      summary = summaryStore.getSummary();
      expect(summary.length).to.equal(0);
    });
  });
});
