'use strict';

/**
 * Just a helper file for testing node events
 * @param {string} event - event name three options: [message,error,disconnect]
 */
const nodeChildProcessTest = (event) => {

  /** @type {number} */
  let exitCode = 0;

  if (event === 'exit') {
    exitCode = 1;
    throw new global.Error(1); // Error
  } else if (event === 'message') {
    const summary = {
      'name': `Testing`,
      'time': new Date().getSeconds(),
      'results': 'SUCCESS',
    };

    // Updating summary reporting
    process.send && process.send(JSON.stringify(summary));
  } else if (event === 'disconnect') {
    process.disconnect(); // cancel main
  }

  process.exit(exitCode); // cancel main
};

// We have to switch event types so we need to wait for the main thread to send
// that message
process.on('message', (event) => {
  nodeChildProcessTest(event);
});

module.exports = nodeChildProcessTest;
