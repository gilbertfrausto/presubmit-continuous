'use strict';

const sinon = require('sinon');
const {expect} = require('chai');
const {describe, after, it, beforeEach} = require('mocha');
const logFileDetails = require('../helper/log-file-details');

logFileDetails(__filename, __dirname);

let logger;

describe('Log file details', () => {
  beforeEach(() => {
    logger = sinon.stub(console, 'log');
    logFileDetails(__filename, __dirname);
  });
  afterEach(() => {
    console.log.restore();
  });
  it('directory should be shorten to file name', () => {
    expect(logger.called).to.equal(true);
    expect(logger.args[0][0]).to.equal('log-file-details_test.js');
  });
});
