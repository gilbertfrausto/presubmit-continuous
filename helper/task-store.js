'use strict';

const Store = require('./store');


/**
 * This task store saves data that was created in a main or sub-task
 */
class TaskStore extends Store {

  /**
   * @param {string} name - name of the store to be created
   */
  constructor(name) {
    super(name);
  }

  /**
   * Reference to the name of task store
   * @return {string}
   */
  getTaskStoreName() {
    return this.name;
  }
}


module.exports = TaskStore;
