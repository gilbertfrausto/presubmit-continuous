'use strict';

const {expect} = require('chai');
const sinon = require('sinon');
const cmd = require('node-command-line');
const child_process = require('child_process');
const {describe, beforeEach, it, afterEach,} = require('mocha');
const {GIT, DemosDocsDirectories} = require('../constants');
const logFileDetails = require('../helper/log-file-details');
const directoryGitChanges = require('../helper/directory-git-changes');
const TaskStore = require('../helper/task-store');
const taskStore = new TaskStore('git-store');

logFileDetails(__filename, __dirname); // log file details

let task_1;
let task_2;
let forked;

const success = () => {
  return new Promise((resolve) => {
    resolve({success: true});
  });
};

describe('Directory Git Changes', () => {
  beforeEach((done) => {
    task_1 = sinon.stub(cmd, 'run');
    task_1.callsFake(success);

    task_2 = sinon.spy(taskStore, 'update');
    directoryGitChanges();

    setTimeout(done, 500);
  });

  afterEach(() => {
    taskStore.update.restore();
    cmd.run.restore();
  });

  it('should run correct command', () => {
    const results = [];
    const firstCall = task_1.firstCall.args[0];
    const filter = Object.keys(DemosDocsDirectories)
        .filter(item => firstCall
        .includes(DemosDocsDirectories[item].COMMANDS.GIT_CHANGE));

    task_1.args.forEach(item => results.push(item[0]));

    expect(results.length >= 1).to.equal(true);
    expect(task_1.callCount).to.be.greaterThan(0);
  });

  it('should call task store to update with results', () => {
    const changes = Object.keys(taskStore.select('changed'));
    expect(changes.length).to.be.greaterThan(0);
  });
});
