'use strict';

const cmd = require('node-command-line');
const Promise = require('bluebird');

// This is to help run shell commands and logs results in an non-intrusive way
function debug(task) {
  Promise.coroutine(function* () {
    const response = yield cmd.run(task);
    if (response.success) {
      console.log(`Task ran ${task}: ${response.message}`); // eslint-disable-line no-console
    }
  })();
}

module.exports = debug;
