'use strict';

const {expect} = require('chai');
const {describe, beforeEach, it} = require('mocha');
const parallel = require('../helper/parallel');
const logFileDetails = require('../helper/log-file-details');

logFileDetails(__filename, __dirname); // Log test file name

describe('Parallel counter for child process tracking,', () => {
  beforeEach(() => {
    parallel.setProcessCount(1);
  });

  it('counter should be correctly set to 1 and incremented', () => {
    expect(parallel.getCurrentCount()).to.equal(1);

    // Increase process count
    parallel.addProcess();

    expect(parallel.getCurrentCount()).to.equal(2);
  });

  it('counter should correctly remove process from the count', () => {
    expect(parallel.getCurrentCount()).to.equal(1);

    // Increase process count
    parallel.addProcess();
    parallel.addProcess();

    // Remove Process
    parallel.removeProcess();

    expect(parallel.getCurrentCount()).to.equal(2);
  });

  it('should correctly take param and set count', () => {
    // Setting custom count
    parallel.setProcessCount(10);

    expect(parallel.getCurrentCount()).to.equal(10);
  });
});