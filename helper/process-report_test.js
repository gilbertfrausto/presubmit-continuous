'use strict';

const {expect} = require('chai');
const {describe, beforeEach, it} = require('mocha');
const ProcessReport = require('../helper/process-report');
const logFileDetails = require('../helper/log-file-details');

logFileDetails(__filename, __dirname);

let report;

describe('Process Report and details class', () => {
  beforeEach(() => {
    report = undefined;
  });

  it('should correctly capture process details', () => {
    report = new ProcessReport('XYZ-123', 10, true, false);
    expect(report.getReportDetails()).to.contain('XYZ-123');
    expect(report.getReportDetails()).to.contain('Disconnected');
    expect(report.getReportDetails()).to.contain('10');

    report = new ProcessReport('asdf-123', 100, false, false);
    expect(report.getReportDetails()).to.contain('asdf-123');
    expect(report.getReportDetails()).to.contain('Connected');
    expect(report.getReportDetails()).to.contain('100');
  });
});