'use strict';

const Promise = require('bluebird');
const cmd = require('node-command-line');
const mastHead = require('./task/mast-head');
const PresubmitContinuous = require('./task/presubmit-continuous');
const TaskStore = require('./helper/task-store');
const {Omni, GitCommand} = require('./constants');


/**
 * Main class will call the Mast head method and kick off
 * 'Presubmit' or 'Continuous' based on the flag passed to the constructor
 */
class Main {

  /**
   * @param {boolean=} init If true for will run the init method. This helps
   *    for  unit testing.
   * @param {boolean=} presubmit Needed to distinguish presubmit and continuous
   *    builds.
   */
  constructor(init = true, presubmit = true) {
    /** @const {!mastHead} */
    this.mastHead = mastHead;

    /** @type {boolean} */
    this.build;

    /** @private @const {boolean} */
    this.presubmit_ = presubmit;

    /** @private {!TaskStore} */
    this.keyStore_;

    init && this.init();
  }

  /**
   * Get npm version number and pass it to the @type{Main.start} method. No NPM
   * version is attached to the Global object, but if there is another option we
   * can update this.
   */
  async init() {
    // Update build property
    this.build = this.presubmit_ ? Omni.PRESUBMIT : Omni.CONTINUOUS;

    // Store for the async const values
    this.keyStore_ = new TaskStore('key-store');

    // Start main task after getting all const values
    await this.getAsyncConstValues_();

    // pass NPM value to the start method and kick off scripts
    this.start(this.keyStore_.select('MAIN_CONSTANTS').NPM_V);
  }

  /**
   * Main method to kick off scripts.
   * @param {number} npm_v Npm version number.
   */
  async start(npm_v) {
    await this.mastHead(npm_v); // Build command line mast-header
    await PresubmitContinuous.getInstance(this.presubmit_);
  }

  /**
   * There are a few constants that need to be generated for kokoro & GIT
   * before running any task. If more kokoro or GIT constants values are needed,
   * just pass them to the run command in the task array.
   * @return {!Promise<void>}
   * @private
   */
  getAsyncConstValues_() {

    // Add the properties needed
    const MAIN_CONSTANTS = {
      PERCY_TARGET_BRANCH: Omni.PERCY_TARGET_BRANCH,
    };

    /**
     * Running GIT commands for changes and branch info.
     * @const {!Array<!Promise<*>>}
     */
    const task = [
      cmd.run(GitCommand.LAST_AUTHOR), // USERNAME
      cmd.run(GitCommand.LAST_COMMIT_HASH), // PERCY COMMIT
      cmd.run(Omni.COMMANDS.NPM_VERSION), // NPM Version
    ];

    return Promise.all(task).each((item, i) => {
      if (item.success) {
        if (i === 0) {
          const username = item.message;
          const KGCN = Omni.KOKORO_GERRIT_CHANGE_NUMBER || 'UNKNOWN';

          // Keeping these keys the same
          MAIN_CONSTANTS.USERNAME = username;
          MAIN_CONSTANTS.PERCY_BRANCH = `${username}-cn-${KGCN}`;
          MAIN_CONSTANTS.STAGING_VERSION = `${username}-cr-${KGCN}`;
        } else if (i === 1) {
          MAIN_CONSTANTS.PERCY_COMMIT = item.message;
        } else if (i === 2) {
          MAIN_CONSTANTS.NPM_V = item.message;
        }
      }

      // Update the key store. This is needed for other classes and testing.
      this.keyStore_.update({MAIN_CONSTANTS});

    }).error((e) => {
      console.error(`Invalid shell commands`);
    }).catch((e) => {
      console.error('catch', e);
    });
  }
}


/**
 * Need this to call from package json and this method helps us avoid
 * using the keyword 'new'. Using 'new' gives a syntax error.
 *
 * Usages:
 * $ ~./presubmit-continuous> npm run main
 *
 * @param {boolean=} init If true for will run the init method. This helps for
 *    unit testing.
 * @param {boolean=} presubmit Needed to distinguish presubmit and continuous.
 */
const main = (init = true, presubmit = true) => {
  new Main(init, presubmit);
};

module.exports = {
  Main, main,
};
