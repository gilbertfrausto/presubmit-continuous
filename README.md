### How to run

#### Install packages
```
  $> cd ./test/kokoro/presubmit-continuous/
  $> npm install
```

#### Presubmit
```$> npm run presubmit```

#### Continuous
```$> npm run continuous```

#### Unit-Test
```$> npm test```
