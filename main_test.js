'use strict';

const Promise = require('bluebird');
const cmd = require('node-command-line');
const sinon = require('sinon');
const {Versions, Omni, GitCommand, Command} = require('./constants');
const {expect} = require('chai');
const {before, describe, it, after} = require('mocha');
const logFileDetails = require('./helper/log-file-details');
const {Main} = require('./main');
const PresubmitContinuous = require('./task/presubmit-continuous');
const SummaryStore = require('./helper/summary-store');
const TaskStore = require('./helper/task-store');

let mainInstance;
let start;
let header;
let summaryStore;
let keyStore;
let cmdStub;

const logger = console.log;

logFileDetails(__filename, __dirname);

const success = new Promise((resolve) => {
  resolve({success: true});
});

describe('Main app file', () => {
  describe('continuous', () => {
    // Have to stub most external resources to prevent a real build from running
    before((done) => {
      sinon.stub(console, 'log');
      start = sinon.stub(PresubmitContinuous, 'getInstance').callsFake(sinon.fake());
      mainInstance = new Main(false, false);
      keyStore = new TaskStore('key-store');
      header = sinon.stub(mainInstance, 'mastHead').callsFake(sinon.fake());
      cmdStub = sinon.stub(cmd, 'run').returns(success);
      mainInstance.init();
      setTimeout(done, 500);
    });

    after(() => {
      PresubmitContinuous.getInstance.restore();
      mainInstance.mastHead.restore();
      console.log.restore();
      cmd.run.restore();
    });

    it('should be continuous build', () => {
      expect(mainInstance.build).to.equal(Omni.CONTINUOUS);
    });
  });

  describe('presubmit', () => {
    // Have to stub most external resources to prevent a real build from running
    before((done) => {
      sinon.stub(console, 'log');
      mainInstance = new Main(false, true);
      start = sinon.stub(PresubmitContinuous, 'getInstance').callsFake(sinon.fake());
      header = sinon.stub(mainInstance, 'mastHead').callsFake(sinon.fake());
      summaryStore = sinon.stub(SummaryStore, 'getInstance').returns({
        printSummary: sinon.fake()
      });
      cmdStub = sinon.stub(cmd, 'run').returns(success);
      mainInstance.init();
      setTimeout(done, 1500);
    });

    after(() => {
      mainInstance.mastHead.restore();
      console.log.restore();
      PresubmitContinuous.getInstance.restore();
      cmd.run.restore();
    });

    it('should correctly get node version and call presubmit class', () => {
      expect(mainInstance.build).to.equal(Omni.PRESUBMIT);
      expect(start.called).to.equal(true);
      expect(header.called).to.equal(true);
    });

    it('should correctly run git commands', () => {
      expect(cmdStub.args[0][0]).to.equal(GitCommand.LAST_AUTHOR); // PERCY_COMMIT
      expect(cmdStub.args[1][0]).to.equal(GitCommand.LAST_COMMIT_HASH); // PERCY_BRANCH
      expect(cmdStub.args[2][0]).to.equal(Omni.COMMANDS.NPM_VERSION); // NPM -v
      expect(cmdStub.called).to.equal(true);
    });

    // check store for constant values
    it('should have values saved in key store', () => {
      const MAIN_CONSTANTS = keyStore.select('MAIN_CONSTANTS');
      expect(MAIN_CONSTANTS).to.have.property('USERNAME');
      expect(MAIN_CONSTANTS).to.have.property('PERCY_BRANCH');
      expect(MAIN_CONSTANTS).to.have.property('STAGING_VERSION');
      expect(MAIN_CONSTANTS).to.have.property('PERCY_COMMIT');
      expect(MAIN_CONSTANTS).to.have.property('NPM_V');
    });
  });
});
