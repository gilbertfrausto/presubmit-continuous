'use strict';

const {expect} = require('chai');
const sinon = require('sinon');
const cmd = require('node-command-line');
const {describe, before, it, after} = require('mocha');
const {MainDirectory, Command} = require('../../constants');
const logFileDetails = require('../../helper/log-file-details');
const glueMain = require('./glue-main');

// log file details
logFileDetails(__filename, __dirname);

let task_1;
let task_2;
let task_3;
let DIR_1 = 'GLUE_DEMOS'
let DIR_2 = 'HERCULES_DEMOS';

process.send = (any) => {
  return any;
}
process.disconnect = sinon.fake();

const success = () => {
  return new Promise((resolve) => {
    resolve({success: true});
  });
};

describe('Building for demos and docs directories', () => {
  before(() =>{
    sinon.stub(console, 'log');
    task_1 = sinon.stub(cmd, 'run');
    task_1.callsFake(success);
    task_2 = sinon.stub(process, 'send');
    glueMain();
  });

  after(() => {
    cmd.run.restore();
    process.send.restore();
    console.log.restore();
  });

  it('should run correct command for Glue demos', () => {
    expect(task_1.args[0][0]).to.equal(MainDirectory.GLUE_MAIN.COMMANDS.BUILD);
    expect(task_1.calledOnce).to.equal(true);
  });

});