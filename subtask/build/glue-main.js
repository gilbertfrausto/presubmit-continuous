'use strict';

const cmd = require('node-command-line');
const {MainDirectory, Omni, Command} = require('../../constants');
const SummaryStore = require('../../helper/summary-store');

/**
 * Runs Glue main build command.
 * @param {boolean=} log Set to false if you dont want result printed to the
 * terminal
 * @return {Promise<void>}
 */
const glueMainBuild = (log = true) => {
  /** @const {!SummaryStore} */
  const summaryStore = new SummaryStore('summary-store');

  /** @const {number} */
  const startTime = new Date().getSeconds();

  if (log) {
    // eslint-disable-next-line no-console
    console.log(Command.BUILD, MainDirectory.GLUE_MAIN.NAME);
  }

  return cmd.run(MainDirectory.GLUE_MAIN.COMMANDS.BUILD)
      .then((res) => {
        // Updating summary reporting
        summaryStore.updateSummary(/** !SummaryTask */{
          name: `${Command.BUILD} GLUE_MAIN`,
          time: new Date().getSeconds() - startTime,
          results: Omni.SUCCESS,
        });
        console.log(res.message); // eslint-disable-line no-console
      })
      .catch((err) => {
        summaryStore.updateSummary(/** !SummaryTask */{
          name: `${Command.BUILD} GLUE_MAIN`,
          time: new Date().getSeconds() - startTime,
          results: `${Omni.FAILED} ${err}`,
        });
        throw new Error(err);
      });
};

module.exports = glueMainBuild;
