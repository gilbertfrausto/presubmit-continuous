'use strict';

const cmd = require('node-command-line');
const Promise = require('bluebird');
const {DemosDocsDirectories, Omni, Command} = require('../../constants');


/**
 * Running build commands on the ./subtask/build directory.
 * @param {string} dirName Name of directory being forked.
 */
const demosAndDocs = (dirName) => {
  /** @const {number} */
  const startTime = new Date().getSeconds();

  // Send message to main thread
  process.send &&  process.send(`${Command.BUILD} ${dirName}`);

  Promise.coroutine(function* () {
    // TODO(gfrause) Debug having issues with running this file from a sub
    // directory. debug('cd ../../../ && ls');
    const response = yield cmd.run(DemosDocsDirectories[dirName].COMMANDS.BUILD);

    if (response.success && process) {
      /** @const {!SummaryTask} */
      const summary = {
        'name': `${Command.BUILD} ${dirName}`,
        'time': new Date().getSeconds() - startTime,
        'results': Omni.SUCCESS,
      };

      // Updating summary reporting
      process.send(JSON.stringify(summary));
      process.send(response.message);
    } else {
      /** @const {!SummaryTask} */
      const summary = {
        'name': `${Command.BUILD} ${dirName}`,
        'time': new Date().getSeconds() - startTime,
        'results': `${Omni.FAILED}`,
      };

      // Updating summary reporting
      process.send(JSON.stringify(summary));
    }
    process.disconnect();
  })();
};

// Need to get name of the directory
process.on('message', (name) =>{
  demosAndDocs(name);
});


module.exports = demosAndDocs;
