'use strict';

const cmd = require('node-command-line');
const Promise = require('bluebird');
const {Omni, Command} = require('../../constants');

/**
 * Runs the JS unit test command and returns the results.
 * @return {!Promise<void>}
 */
const jsUnitTest = () => {
  console.log(Command.UNIT_TEST); // eslint-disable-line no-console
  return cmd.run(Omni.COMMANDS.UNIT_TEST_JS)
      .then((res) => {
        console.log(res.message); // eslint-disable-line no-console
      })
      .catch((err) => {
        throw new Error(err);
      });
};

module.exports = jsUnitTest;
