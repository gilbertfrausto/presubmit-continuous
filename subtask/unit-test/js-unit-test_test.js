'use strict';

const Promise = require('bluebird');
const cmd = require('node-command-line');
const sinon = require('sinon');
const {Versions, Omni} = require('../../constants');
const {expect} = require('chai');
const {before, describe, it, after} = require('mocha');
const logFileDetails = require('../../helper/log-file-details');
const jsUnitTest = require('./js-unit-test');

let cmdStub;
let keyStore;

const success = new Promise((resolve) => {
  resolve({success: true});
});

logFileDetails(__filename, __dirname);

describe('JS unit test file', () => {

  before((done) => {
    sinon.stub(console, 'log');
    cmdStub = sinon.stub(cmd, 'run').returns(success);
    jsUnitTest();
    setTimeout(done, 100);
  });

  after(() => {
    console.log.restore();
    cmd.run.restore();
  });

  it('should correctly run cdn unit test command', () => {
    expect(cmdStub.args[0][0]).to.equal(Omni.COMMANDS.UNIT_TEST_JS);
    expect(cmdStub.called).to.equal(true);
  });
});


