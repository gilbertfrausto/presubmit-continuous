'use strict';

const cmd = require('node-command-line');
const Promise = require('bluebird');
const {Omni, Command} = require('../../constants');

/**
 * Runs the scss linter command and returns the results.
 * @return {!Promise<void>}
 */
const scssLinter = () => {
  console.log(Command.PRE_TEST); // eslint-disable-line no-console
  return cmd.run(Omni.COMMANDS.PRE_TEST_SCSS)
      .then((res) => {
        console.log(res.message); // eslint-disable-line no-console
      })
      .catch((err) => {
        throw new Error(err);
      });
};

module.exports = scssLinter;
