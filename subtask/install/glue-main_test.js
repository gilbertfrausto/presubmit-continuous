'use strict';

const {expect} = require('chai');
const sinon = require('sinon');
const child_process = require('child_process');
const {describe, beforeEach, it, afterEach} = require('mocha');
const {MainDirectory} = require('../../constants');
const logFileDetails = require('../../helper/log-file-details');
const glueMain = require('./glue-main');

// log file details
logFileDetails(__filename, __dirname);

let task_1;

process.send = (any) => {
  return any;
};

const success = () => {
  return new Promise((resolve) => {
    resolve({success: true});
  });
};

describe('Glue Main install', () => {
  beforeEach(() => {
    task_1 = sinon.stub(child_process, 'exec');
    glueMain(false);
  });

  afterEach(() => {
    child_process.exec.restore();
  });

  it('should run correct command', () => {
    expect(task_1.args[0][0]).to.equal(MainDirectory.GLUE_MAIN.COMMANDS.INSTALL);
    expect(task_1.calledOnce).to.equal(true);
  });
});