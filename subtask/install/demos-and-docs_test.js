'use strict';

const {expect} = require('chai');
const sinon = require('sinon');
const cmd = require('node-command-line');
const {describe, beforeEach, it, after} = require('mocha');
const {DemosDocsDirectories} = require('../../constants');
const logFileDetails = require('../../helper/log-file-details');
const demosAndDocs = require('./demos-and-docs');

// log file details
logFileDetails(__filename, __dirname);

let task_1;
let task_2;
let task_3;
let DIR_1 = 'GLUE_DEMOS'
let DIR_2 = 'HERCULES_DEMOS';

process.send = (any) => {
  return any;
}
process.disconnect = sinon.fake();

const success = () => {
  return new Promise((resolve) => {
    resolve({success: true});
  });
};

describe('Installing for demos and docs directories', () => {
  beforeEach(() => {
    task_1 = sinon.stub(cmd, 'run');
    task_1.callsFake(success);
    task_2 = sinon.stub(process, 'send');
  });

  afterEach(() => {
    cmd.run.restore();
    process.send.restore();
  });

  it('should run correct command for Glue demos', () => {
    demosAndDocs(DIR_1);
    expect(task_1.args[0][0]).to.equal(DemosDocsDirectories[DIR_1].COMMANDS.INSTALL);
    expect(task_1.calledOnce).to.equal(true);
    expect(task_2.args[0][0]).to.equal(DIR_1);
    expect(task_2.calledOnce).to.equal(true);
  });

  it('should run correct command for Hercules demos', () => {
    demosAndDocs(DIR_2);
    expect(task_1.args[0][0]).to.equal(DemosDocsDirectories[DIR_2].COMMANDS.INSTALL);
    expect(task_1.calledOnce).to.equal(true);
    expect(task_2.args[0][0]).to.equal(DIR_2);
    expect(task_2.calledOnce).to.equal(true);
  });
});