'use strict';

const child_process = require('child_process');
const {MainDirectory, Omni, Command} = require('../../constants');
const SummaryStore = require('../../helper/summary-store');

/**
 * Running install command in the root glue/ directory.
 * @param {boolean=} log Flag that is true by default and will log details.
 *    after completed.
 * @return {!Promise<*>}
 */
const glueMain = (log = true) => {
   /** @const {!SummaryStore} */
   const summaryStore = new SummaryStore('summary-store');

   /** @const {number} */
   const startTime = new Date().getSeconds();

   /** @const {string} */
   const name =  Object.keys(MainDirectory)[0];

  if (log) {
    // eslint-disable-next-line no-console
    console.log(`${Command.INSTALL}: ${name}`);
  }

  return new Promise((resolve, reject) => {
    try {
      /** @const {string} */
      const shellCommand = MainDirectory.GLUE_MAIN.COMMANDS.INSTALL;

      // Execute shell command
      child_process.exec(shellCommand, [], (err, lnOutput, stderr) => {
        if (err) {
          reject(err);
          return;
        }

        // Log messages
        if (log) {
          console.log(lnOutput); // eslint-disable-line no-console
          console.log(stderr); // eslint-disable-line no-console
        }

        // Updating summary reporting
        summaryStore.updateSummary(/** !SummaryTask */{
          name: `${Command.INSTALL} ${name}`,
          time: new Date().getSeconds() - startTime,
          results: Omni.SUCCESS,
        });

        // Resolve the promise
        resolve();
      });
    } catch (e) {
      // Updating summary reporting with error
      summaryStore.updateSummary(/** !SummaryTask */{
        name: `${Command.INSTALL} ${name}`,
        time: new Date().getSeconds() - startTime,
        results: `${Omni.FAILED} ${e}`,
      });
      reject(e);
    }
  });
};

module.exports = glueMain;
