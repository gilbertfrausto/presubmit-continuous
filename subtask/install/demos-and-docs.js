'use strict';

const cmd = require('node-command-line');
const Promise = require('bluebird');
const {DemosDocsDirectories, Omni, Command} = require('../../constants');

/**
 * Running install commands on the ./subtask/install directory
 * @param {string} dirName - name of directory being forked
 */
const demosAndDocs = (dirName) => {
  /** @const {number} */
  const startTime = new Date().getSeconds();

  // process.send will throw exception if not sub threads are running
  process.send && process.send(dirName);

  Promise.coroutine(function* () {
    const response = yield cmd.run(DemosDocsDirectories[dirName]
        .COMMANDS.INSTALL);

    if (response.success && process) {
      /** @const {!SummaryTask} */
      const summary = {
        'name': `${Command.INSTALL} ${dirName}`,
        'time': new Date().getSeconds() - startTime,
        'results': Omni.SUCCESS,
      };

      // Updating summary reporting
      process.send(JSON.stringify(summary));
      process.send(response.message);

    } else {
      /** @const {!SummaryTask} */
      const summary = {
        'name': `${Command.INSTALL} ${dirName}`,
        'time': new Date().getSeconds() - startTime,
        'results': `${Omni.FAILED}`,
      };

      // Updating summary reporting
      process.send(JSON.stringify(summary));
    }
    process.disconnect();
  })();
};

process.on('message', (name) =>{
  demosAndDocs(name);
});

module.exports = demosAndDocs;
