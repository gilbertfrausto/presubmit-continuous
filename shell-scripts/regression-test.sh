# When a porcupine emoji exists, replace blowfish.
echo -e '\n\n*** 🐡 VISUAL REGRESSION TESTS 🐡  ***'

# Set PERCY_BRANCH in continuous.sh and presubmit.sh
echo "PERCY_BRANCH = ${PERCY_BRANCH}"
# Read token from keystore. See b/62536973#comment11 and following CLs for
# how this token is stored.
# Note that a different token is used for each percy project. For convenience
# we're using one project for all of glue.
export PERCY_TOKEN=$(cat -- "${KOKORO_KEYSTORE_DIR}/72736_glue_percy_token")
export PERCY_PROJECT='Google-Glue-team/glue'

# Set Protractor to v5.4.2 to avoid breaking changes from v6.
sudo yarn global add protractor@5.4.2

# TODO(luzhangg): Remove the version number once Kokoro team updates
# the version of Chrome running on VMs. b/75971667
sudo webdriver-manager update --versions.chrome=2.36
google-chrome-stable --version
PROTRACTOR_V=$(protractor --version)
echo "Protractor ${PROTRACTOR_V}"

protractor hercules/test/protractor/percy.protractor.conf  & PERCY_PID=$!
echo "Hercules visual regression test process is ${PERCY_PID}"

# error_handling 'Visual regression tests'