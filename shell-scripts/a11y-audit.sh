#
# This may be needed in this file
#
# export PERCY_COMMIT=$(git log --format="%H" -n 1)
# export PERCY_BRANCH="master"
# export PERCY_TARGET_BRANCH="master"

# GLUEDEMO_CHANGED="cdn/src lib demos scss ':(exclude)lib/*_test.js'";
# GLUEDOCS_CHANGED='docs';
# HERCULESDEMO_CHANGED="cdn/src lib scss hercules/lib hercules/demos";
# HERCULESDOCS_CHANGED="hercules/docs";



# Run accessibility audit on Hercules Demo app and generate an HTML report into
# the audit directory
#!/bin/bash

# Fail on any error.
set -euo pipefail

# Display commands being run.
set -x

run_hercules_accessibility_audit() {
  # Activate service account
  sudo /opt/google-cloud-sdk/bin/gcloud auth activate-service-account \
    --project "hercules-demo" \
    --key-file="../../keystore/72736_hercules_demo_service_account_key"

  # Go to Hercules audit
  cd hercules/audit

  # Run accessibility audit on the deployed Hercules demo app
  # (https://hercules-demo.appspot.com/) and create the report in the report
  # directory
  yarn run test:hercules-a11y -e 'error' --output-directory hercules/audit/report

  # Get the service list from GAE and find accessibility-report service,
  # checking if there is any previous report deployed
  check_service=$(gcloud app services list --filter="SERVICE=accessibility-report")

  # If there is any previous report delete it before we deploy the new one
  if [[ ${check_service} ]]; then
    # Delete accessibility-report service
    gcloud app services delete accessibility-report
  fi

  # Deploy it as a service called accessibility-report, based on the config in
  # the app.yaml file
  gcloud app deploy --project hercules-demo

  echo -e "Accessibility audit was triggered on Hercules. See the report \n
  at https://accessibility-report-dot-hercules-demo.appspot.com"

  cd ../..
}


run_glue_accessibility_audit() {
  # Activate service account
  sudo /opt/google-cloud-sdk/bin/gcloud auth activate-service-account \
    --project "glue-demo" \
    --key-file="../../keystore/72736_glue_demo_service_account_key"

  # Go to audit
  cd audit

  # Run accessibility audit on the deployed Glue Demo app
  # (https://glue-demo.appspot.com/) and create the report in the report
  # directory
  yarn run test:glue-a11y -e 'error' --output-directory audit/report

  # Get the service list from GAE and find accessibility-report service,
  # checking if there is any previous report deployed
  check_service=$(gcloud app services list --filter="SERVICE=accessibility-report")

  # If there is any previous accessibility-report service delete it before we
  # deploy the new one
  if [[ ${check_service} ]]; then
    # Delete accessibility-report service
    gcloud app services delete accessibility-report
  fi

  # Deploy it as a service called accessibility-report, based on the config in
  # the app.yaml file
  gcloud app deploy --project glue-demo

  echo -e "Accessibility audit was triggered for Glue. See the report at \n
  https://accessibility-report-dot-glue-demo.appspot.com"

  cd ..
}

run_hercules_accessibility_audit
run_glue_accessibility_audit
