# It deploys Glue Demo, Hercules Demo, Glue Docs and Hercules Docs applications.
# It is triggered by continuous.sh

#!/bin/bash
# Fail on any error.
set -e
# Display commands being run.
set -x

auto_deploy() {
    # This is required for fixing the gcloud permission issue. (b/121382207)
    gcloud auth list

    # Activate service account
    sudo /opt/google-cloud-sdk/bin/gcloud auth activate-service-account \
      --project "glue-demo" \
      --key-file="../../keystore/72736_glue_demo_service_account_key"

    # Go to Glue demo
    cd demos
    # Deploy Glue demo
    yarn deploy
    cd ..

    # Activate service account
    sudo /opt/google-cloud-sdk/bin/gcloud auth activate-service-account \
      --project "glue-docs" \
      --key-file="../../keystore/72736_glue_doc_service_account_key"

    # Go to Glue docs
    cd docs
    # Deploy Glue docs
    yarn deploy
    cd ..

    # Activate service account
    sudo /opt/google-cloud-sdk/bin/gcloud auth activate-service-account \
      --project "hercules-demo" \
      --key-file="../../keystore/72736_hercules_demo_service_account_key"

    # Go to Hercules demo
    cd hercules/demos
    # Deploy Hercules demo
    yarn deploy
    cd ../..

    # Activate service account
    sudo /opt/google-cloud-sdk/bin/gcloud auth activate-service-account \
      --project "hercules-docs" \
      --key-file="../../keystore/72736_hercules_docs_service_account_key"

    # Go to Hercules docs
    cd hercules/docs
    # Deploy Hercules docs
    yarn deploy
    cd ../..
}

auto_deploy